/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	var _com = __webpack_require__(1);

	var _com2 = _interopRequireDefault(_com);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var components = (0, _com2.default)(['HeaderLayout', 'HeaderDock', 'LayoutMain', 'LayoutBox', 'Btn', 'Icon', 'FormGroup', 'FormInput', 'GridRow']);

	//

	new Vue({
		el: '#app',
		components: components,
		methods: {
			submit: function submit() {
				this.$broadcast('form-verify', '');
				if (this.$refs.form.pass) {
					console.log('sending data');
					console.log(this.$refs.form.data);
					var _this = this;
					$.ajax({
						url: SITE_URL + '/user/logining',
						type: 'post',
						data: this.$refs.form.data,
						success: function success(data) {
							if (!data.state) {
								_this.$refs.form.setNotice(data.param);
							} else {
								window.location = SITE_URL + '/home';
							}
						},
						error: function error() {},
						dataType: 'json'
					});
				} else {
					//this.$refs.myform.setNotice('账号密码不正确')
					console.log('data wrong');
				}
			}
		}
	});

/***/ },
/* 1 */
/***/ function(module, exports) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
		value: true
	});

	exports.default = function (components) {

		var res = {};
		for (var i = 0; i < components.length; i++) {
			res[components[i]] = Bone[components[i]];
		}
		return res;
	};

/***/ }
/******/ ]);