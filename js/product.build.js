/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	var _com = __webpack_require__(1);

	var _com2 = _interopRequireDefault(_com);

	var _urly = __webpack_require__(4);

	var _urly2 = _interopRequireDefault(_urly);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var components = (0, _com2.default)(['HeaderLayout', 'HeaderDock', 'LayoutMain', 'LayoutBox', 'LayoutSideMenu', 'TabsSide', 'TabsItemSide', 'Icon', 'List', 'ListItem', 'Product', 'ShoppingDock', 'FormStars', 'FormCheck', 'GridRow', 'GridCol']);

	//cvx
	new Vue({
		el: '#app',
		components: components,
		data: function data() {
			return {
				currentDate: '',
				currentDay: '',
				total: TOTAL,
				week: ['一', '二', '三', '四', '五', '六', '日'],
				comments: [],
				commentsPageSize: 3,
				commentsRequest: null
			};
		},

		ready: function ready() {
			var _this = this;
			var url = new _urly2.default(window.location.href);
			this.currentDate = url.getParams().date;
			var d = new Date(this.currentDate);

			this.currentDay = d.getDay() - 1;

			this.loadingComments({
				pageSize: this.commentsPageSize,
				page: 0
			});
		},
		events: {},
		methods: {
			loadingComments: function loadingComments(param) {
				var _this = this;
				_this.$refs.comments.setLoading();

				if (_this.commentsRequest) {
					_this.commentsRequest.abort();
				}
				_this.commentsRequest = $.ajax({
					url: SITE_URL + '/home/get_productcomment',
					type: 'get',
					data: {
						productid: PRODUCT.id,
						page: param.pageSize,
						offset: param.pageSize * param.page
					},
					success: function success(data) {
						_this.$refs.comments.addData(data, 'id');
						_this.comments = _this.$refs.comments.getData();
						_this.$refs.comments.reSet();
						if (_this.comments.length == 0) {
							_this.$refs.comments.setEnd('没有评价');
						}
						if (data.length < _this.commentsPageSize) {
							_this.$refs.comments.setEnd();
						}
					},
					error: function error() {}
				});
			},
			buy: function buy(check) {

				if (!check.checked) {
					check.setChecked(true);
					return;
				}
				var _this = this;
				$.ajax({
					url: SITE_URL + '/shoppingcart/addtocart',
					data: {
						productid: PRODUCT.id,
						amount: 1,
						date: _this.currentDate
					},
					type: 'post',
					success: function success(data) {
						if (data.code) {
							_this.total = data.data;
						} else {
							alert(data.msg);
						}
					},
					error: function error() {
						//alert('网络错误，请重试');
					}

				});
			},
			gotoshp: function gotoshp() {
				window.location = SITE_URL + '/shoppingcart';
			}
		}
	});

/***/ },
/* 1 */
/***/ function(module, exports) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
		value: true
	});

	exports.default = function (components) {

		var res = {};
		for (var i = 0; i < components.length; i++) {
			res[components[i]] = Bone[components[i]];
		}
		return res;
	};

/***/ },
/* 2 */,
/* 3 */,
/* 4 */
/***/ function(module, exports) {

	'use strict';

	var urly = function urly(href) {
		this.href = '';
		this.href2 = '';
		this.paramsString = '';
		this.params = {};

		this.setHref = function () {};

		this.getHref = function () {
			return this.href;
		};

		this.setParams = function (params) {
			for (key in params) {
				//if(this.params[key] != undefined){
				if (key != '' && key != undefined && params[key] != '' && params[key] != undefined) this.params[key] = params[key];
				if (this.params[key] != undefined && params[key] == '' || params[key] == undefined) delete this.params[key];
			}
			var p = '';
			var i = 0;
			for (key in this.params) {
				if (key != '' && key != undefined) {
					if (i == 0) {
						p = p + key + '=' + this.params[key];
					} else {
						p = p + '&' + key + '=' + this.params[key];
					}
					i++;
				}
			}
			this.paramsString = p;
			this.href = this.href2 + '?' + this.paramsString;
		};

		this.init = function (href) {

			this.href = href;
			if (this.href.indexOf('?') >= 0) {
				this.paramsString = this.href.split('?')[1];
				this.href2 = this.href.split('?')[0];
			} else {
				this.href2 = href;
			}
			this.parseParams();
		};

		//return array
		this.parseParams = function () {
			var par = this.paramsString.split('&');
			var size = par.length;

			while (size--) {
				//console.log(size);
				var p = par[size];
				//console.log(p)
				p = p.split('=');
				this.params[p[0]] = p[1] ? p[1] : '';
			}
		};

		this.getParams = function () {
			return this.params;
		};

		this.init(href);
	};

	module.exports = urly;

/***/ }
/******/ ]);