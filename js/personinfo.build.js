/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	var _com = __webpack_require__(1);

	var _com2 = _interopRequireDefault(_com);

	var _selecthelper = __webpack_require__(2);

	var _selecthelper2 = _interopRequireDefault(_selecthelper);

	var _schools = __webpack_require__(3);

	var _schools2 = _interopRequireDefault(_schools);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var components = (0, _com2.default)(['HeaderLayout', 'HeaderDock', 'LayoutMain', 'LayoutBox', 'Btn', 'Icon', 'FormGroup', 'FormInput', 'FormSelect', 'FormCode', 'GridRow']);

	new Vue({
		el: '#app',
		components: components,
		created: function created() {
			var _this = this;

			var d = [];
			for (var i = 0; i < SCHOOLS.length; i++) {
				var item = {
					val: SCHOOLS[i].id,
					text: SCHOOLS[i].name
				};
				if (SCHOOLS[i].id == USERINFO.school) {
					item.selected = 'selected';
				}
				d.push(item);
			}
			_this.schools = d;

			_this.grades = _selecthelper2.default.translate(GRADES, {
				val: 'id',
				text: 'name'
			}, USERINFO.grade);
			_this.classes = _selecthelper2.default.translate(CLASSES, {
				val: 'id',
				text: 'name'
			}, USERINFO.class);
			//console.log(this.schools)
		},
		data: function data() {
			return {
				userinfo: USERINFO,
				schools: [],
				grades: [],
				classes: []
			};
		},

		methods: {
			schoolchange: function schoolchange(obj) {
				var _this = this;
				var type = '';
				var sh = this.schools;
				this.grades = [];
				this.classes = [];
				this.schoolid = obj.val;
				this.$refs.grades.setVal('0');
				this.$refs.classes.setVal('0');
				for (var key = 0; key < sh.length; key++) {
					if (this.schools[key].val == obj.val) {
						type = this.schools[key].type;
						this.schooltype = type;
					}
				}
				_schools2.default.getGrades(SITE_URL + '/passport/getgrades', {
					type: type
				}, function (d) {
					_this.grades = d;
				});
			},
			gradechange: function gradechange(obj) {
				var _this = this;
				this.classes = [];
				this.$refs.classes.setVal('0');
				_schools2.default.getClasses(SITE_URL + '/passport/getclasses', {
					school: _this.schoolid,
					grade: obj.val
				}, function (d) {
					_this.classes = d;
				});
			},
			submit: function submit() {
				this.$broadcast('form-verify', '');
				if (this.$refs.form.pass) {
					console.log('sending data');
					console.log(this.$refs.form.data);
					$.ajax({
						url: SITE_URL + '/user/update',
						type: 'post',
						data: this.$refs.form.data,
						success: function success(data) {
							if (!data.code) {
								alert(data.msg);
							} else {
								alert(data.msg);
							}
						},
						error: function error() {
							alert('系统错误');
						}
					});
				} else {
					//this.$refs.myform.setNotice('账号密码不正确')
					console.log('data wrong');
				}
			}
		}
	});

/***/ },
/* 1 */
/***/ function(module, exports) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
		value: true
	});

	exports.default = function (components) {

		var res = {};
		for (var i = 0; i < components.length; i++) {
			res[components[i]] = Bone[components[i]];
		}
		return res;
	};

/***/ },
/* 2 */
/***/ function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
		value: true
	});
	exports.default = {
		translate: function translate(data, config, selected) {
			var d = [];
			for (var i = 0; i < data.length; i++) {
				var item = {
					val: data[i][config.val],
					text: data[i][config.text]
				};
				d.push(item);
			}
			if (selected) {
				for (var i = 0; i < d.length; i++) {
					if (d[i].val == selected) {
						d[i].selected = 'selected';
					} else {
						delete d[i].selected;
					}
				}
			}

			return d;
		}
	};

/***/ },
/* 3 */
/***/ function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
		value: true
	});
	exports.default = {
		getSchools: function getSchools(url, _success) {
			$.ajax({
				url: url,
				type: 'get',
				success: function success(res) {
					if (res.code) {

						var d = [];
						for (var i = 0; i < res.data.length; i++) {
							d.push({
								val: res.data[i].id,
								text: res.data[i].name,
								type: res.data[i].type
							});
						}
						_success(d);
					}
				},
				error: function error() {
					alert('网络错误');
				}
			});
		},
		getGrades: function getGrades(url, data, _success2) {
			$.ajax({
				url: url,
				type: 'post',
				data: data,
				success: function success(res) {
					console.log(res);
					if (res.code) {

						var d = [];
						for (var i = 0; i < res.data.length; i++) {
							d.push({
								val: res.data[i].id,
								text: res.data[i].name,
								type: res.data[i].type
							});
						}
						_success2(d);
					}
				},
				error: function error() {
					alert('网络错误');
				}
			});
		},
		getClasses: function getClasses(url, data, _success3) {
			$.ajax({
				url: url,
				type: 'post',
				data: data,
				success: function success(res) {
					console.log(res);
					if (res.code) {

						var d = [];
						for (var i = 0; i < res.data.length; i++) {
							d.push({
								val: res.data[i].id,
								text: res.data[i].name,
								type: res.data[i].type
							});
						}
						_success3(d);
					}
				},
				error: function error() {
					alert('网络错误');
				}
			});
		}
	};

/***/ }
/******/ ]);