/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	var _com = __webpack_require__(1);

	var _com2 = _interopRequireDefault(_com);

	var _schools = __webpack_require__(2);

	var _schools2 = _interopRequireDefault(_schools);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var components = (0, _com2.default)(['HeaderLayout', 'HeaderDock', 'LayoutMain', 'LayoutBox', 'Btn', 'Icon', 'FormGroup', 'FormInput', 'FormSelect', 'FormCode', 'GridRow', 'TabsHorizon', 'TabsItemHorizon', 'TabsVertical', 'TabsItemVertical']);

	//jj
	new Vue({
		el: '#app',
		components: components,
		created: function created() {
			var _this = this;

			_this.getdata();

			$.ajax({
				url: SITE_URL + '/category/get_all',
				type: 'get',
				data: {
					type: 1

				},
				success: function success(data) {
					if (data.state == undefined) {
						var d = [];
						for (var i = 0; i < data.length; i++) {
							d.push({
								val: data[i].cid,
								text: data[i].name
							});
						}

						_this.category = d;
					}
				},
				dataType: 'json'
			});
		},
		data: function data() {
			return {
				category: [],
				type1: [],
				type2: [],
				data: [],
				total: 0,
				category: {}
			};
		},

		methods: {
			getdata: function getdata(mon) {
				var _this = this;
				$.ajax({
					url: SITE_URL + '/home/get_payment',
					type: 'get',
					data: {
						limit: 100000,
						offset: 0,
						cid: 0
					},
					success: function success(data) {

						var tt = 0;
						for (var i = 0; i < data.length; i++) {
							var dd = new Date(data[i].time * 1000);
							data[i].time = dd.getFullYear() + '-' + (dd.getMonth() + 1) + '-' + dd.getDate();
							tt += parseFloat(data[i].count);
						}
						_this.data = data;
						_this.total = tt.toFixed(2);
					},
					error: function error() {
						//alert('系统错误')
					},
					dataType: 'json'
				});
			},

			submit: function submit() {
				var _this = this;
				this.$broadcast('form-verify', '');
				if (this.$refs.form.pass) {
					console.log('sending data');
					console.log(this.$refs.form.data);
					var params = this.$refs.form.data;
					params.time = new Date().getTime() / 1000;
					$.ajax({
						url: SITE_URL + '/home/save_payment',
						type: 'get',
						data: params,
						success: function success(data) {
							console.log(data);
							if (!data.state) {

								alert(data.param);
							} else {
								alert('添加成功');
								window.location = SITE_URL + '/payment';
							}
						},
						error: function error() {
							//alert('系统错误')
						},
						dataType: 'json'
					});
				} else {
					//this.$refs.myform.setNotice('账号密码不正确')
					console.log('data wrong');
				}
			}
		}
	});

/***/ },
/* 1 */
/***/ function(module, exports) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
		value: true
	});

	exports.default = function (components) {

		var res = {};
		for (var i = 0; i < components.length; i++) {
			res[components[i]] = Bone[components[i]];
		}
		return res;
	};

/***/ },
/* 2 */
/***/ function(module, exports) {

	'use strict';

	Object.defineProperty(exports, "__esModule", {
		value: true
	});
	exports.default = {
		getSchools: function getSchools(url, _success) {
			$.ajax({
				url: url,
				type: 'get',
				success: function success(res) {
					if (res.code) {

						var d = [];
						for (var i = 0; i < res.data.length; i++) {
							d.push({
								val: res.data[i].id,
								text: res.data[i].name,
								type: res.data[i].type
							});
						}
						_success(d);
					}
				},
				error: function error() {
					alert('网络错误');
				}
			});
		},
		getGrades: function getGrades(url, data, _success2) {
			$.ajax({
				url: url,
				type: 'post',
				data: data,
				success: function success(res) {
					console.log(res);
					if (res.code) {

						var d = [];
						for (var i = 0; i < res.data.length; i++) {
							d.push({
								val: res.data[i].id,
								text: res.data[i].name,
								type: res.data[i].type
							});
						}
						_success2(d);
					}
				},
				error: function error() {
					alert('网络错误');
				}
			});
		},
		getClasses: function getClasses(url, data, _success3) {
			$.ajax({
				url: url,
				type: 'post',
				data: data,
				success: function success(res) {
					console.log(res);
					if (res.code) {

						var d = [];
						for (var i = 0; i < res.data.length; i++) {
							d.push({
								val: res.data[i].id,
								text: res.data[i].name,
								type: res.data[i].type
							});
						}
						_success3(d);
					}
				},
				error: function error() {
					alert('网络错误');
				}
			});
		}
	};

/***/ }
/******/ ]);