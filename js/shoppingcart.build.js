/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	var _com = __webpack_require__(1);

	var _com2 = _interopRequireDefault(_com);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var components = (0, _com2.default)(['HeaderLayout', 'HeaderDock', 'LayoutMain', 'LayoutBox', 'LayoutSideMenu', 'TabsSide', 'TabsItemSide', 'Icon', 'List', 'ListItem', 'Product2', 'ShoppingDock', 'Btn']);

	//cvxd
	new Vue({
		el: '#app',
		components: components,
		data: function data() {
			return {
				//data: [{data:[]},{data:[]},{data:[]},{data:[]},{data:[]},{data:[]},{data:[]}],
				data: [{ data: [] }, { data: [] }, { data: [] }, { data: [] }, { data: [] }],
				request: null,
				total: 0
			};
		},

		created: function created() {
			var haha = ['一', '二', '三', '四', '五', '六', '日'];
			var priceall = 0;
			for (var i = 0; i < PRODUCTS.length; i++) {
				var date = new Date(PRODUCTS[i].date);
				var day = date.getDay();

				if (day && day > 0 && day <= 5) {
					var m = date.getMonth() + 1;
					if (day == 0) {
						this.data[6].data.push(PRODUCTS[i]);
						this.data[6].date = '周日 (' + m + '月' + date.getDate() + '日)';
					} else {
						this.data[day - 1].data.push(PRODUCTS[i]);

						this.data[day - 1].date = '周' + haha[day - 1] + ' (' + m + '月' + date.getDate() + '日)';
					}

					priceall += PRODUCTS[i].price * PRODUCTS[i].amount;
				}
			}

			this.total = PRODUCTS.length;
			for (var i = 0; i < this.data.length; i++) {
				if (this.data[i].data.length == 0) {
					this.data[i].empty = true;
					this.data[i].date = '周' + haha[i];
				} else {
					this.data[i].empty = false;
				}
			}
			console.log(this.data);
		},
		events: {},
		methods: {
			pay: function pay() {
				$.ajax({
					url: SITE_URL + '/order/save/' + $('#code').val(),
					type: 'post',
					success: function success(data) {
						if (!data.code) {
							alert(data.msg);
						} else {
							window.location = SITE_URL + '/order/detail/id/' + data.data;
						}
					},
					error: function error() {
						alert('网络发生错误，请重试');
					}
				});
			},
			clean: function clean() {

				if (confirm('确定清空购物车')) {
					$.ajax({
						url: SITE_URL + '/shoppingcart/clear',
						type: 'get',
						success: function success(data) {
							if (data.code) {
								window.location.reload();
							} else {
								alert(data.msg);
							}
						},
						error: function error() {
							//alert('网络错误，请重试');
						}

					});
				}
			},
			delete: function _delete(pra) {
				this.request = $.ajax({
					url: SITE_URL + '/shoppingcart/delete',
					data: {
						id: pra.id
					},
					type: 'post',
					success: function success(data) {
						if (data.code) {
							window.location.reload();
						} else {
							alert(data.msg);
						}
					},
					error: function error() {
						//alert('网络错误，请重试');
					}

				});
				return;
			},
			buy: function buy(pra) {
				if (this.request) {
					this.request.abort();
				}
				var _this = this;

				if (pra.counter.val == 0) {

					this.request = $.ajax({
						url: SITE_URL + '/shoppingcart/delete',
						data: {
							id: pra.id
						},
						type: 'post',
						success: function success(data) {
							if (data.code) {
								window.location.reload();
							} else {
								alert(data.msg);
							}
						},
						error: function error() {
							//alert('网络错误，请重试');
						}

					});
					return;
				}

				this.request = $.ajax({
					url: SITE_URL + '/shoppingcart/updatecart',
					data: {
						id: pra.id,
						amount: pra.counter.val
					},
					type: 'post',
					success: function success(data) {
						if (data.code) {
							_this.total = data.data;
						} else {
							alert(data.msg);
						}
					},
					error: function error() {
						//alert('网络错误，请重试');
					}

				});
			}
		}
	});

/***/ },
/* 1 */
/***/ function(module, exports) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
		value: true
	});

	exports.default = function (components) {

		var res = {};
		for (var i = 0; i < components.length; i++) {
			res[components[i]] = Bone[components[i]];
		}
		return res;
	};

/***/ }
/******/ ]);