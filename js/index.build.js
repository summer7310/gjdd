/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	var _com = __webpack_require__(1);

	var _com2 = _interopRequireDefault(_com);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var components = (0, _com2.default)(['HeaderLayout', 'HeaderDock', 'LayoutMain', 'LayoutBox', 'LayoutSideMenu', 'TabsSide', 'TabsItemSide', 'TabsVertical', 'TabsItemVertical', 'Icon', 'List', 'ListItem', 'Product', 'ShoppingDock', 'MenuSquare']);

	//cvxfsd
	new Vue({
		el: '#app',
		components: components,
		data: function data() {
			return {
				data: {
					income: 0,
					payment: 0
				}
			};
		},

		ready: function ready() {
			var today = new Date();
			var year = today.getFullYear();
			var month = today.getMonth();
			var _this = this;

			var start = new Date(year, month, 0, 0, 0, 0).getTime() / 1000;
			var end = new Date(year, month + 1, 0, 0, 0, 0).getTime() / 1000;
			$.ajax({
				url: SITE_URL + '/home/get_data',
				type: 'get',
				data: {
					start: start,
					end: end
				},
				success: function success(data) {

					_this.data = data;
				},
				dataType: 'json'
			});
		},
		methods: {}
	});

/***/ },
/* 1 */
/***/ function(module, exports) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
		value: true
	});

	exports.default = function (components) {

		var res = {};
		for (var i = 0; i < components.length; i++) {
			res[components[i]] = Bone[components[i]];
		}
		return res;
	};

/***/ }
/******/ ]);