/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	var _com = __webpack_require__(1);

	var _com2 = _interopRequireDefault(_com);

	function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

	var components = (0, _com2.default)(['HeaderLayout', 'HeaderDock', 'LayoutMain', 'LayoutBox', 'TabsVertical', 'TabsItemVertical', 'Icon', 'GridRow', 'Btn']);

	//1f
	new Vue({
		el: '#app',
		components: components,
		created: function created() {
			var _this = this;
			$.ajax({
				url: SITE_URL + '/user/getall',
				type: 'get',
				data: {
					type: 1

				},
				success: function success(data) {
					if (data.state == undefined) {

						_this.users = data;
						console.log(_this.users);
					}
				},
				dataType: 'json'
			});
		},
		data: function data() {
			return {
				users: [],
				roles: [{
					val: 1,
					text: '父亲'
				}, {
					val: 2,
					text: '母亲'
				}, {
					val: 3,
					text: '儿子'
				}, {
					val: 4,
					text: '女儿'
				}]
			};
		},

		methods: {
			delete: function _delete(id) {
				$.ajax({
					url: SITE_URL + '/user/delete',
					type: 'get',
					data: {
						uid: id
					},
					success: function success(data) {
						console.log(data);
						if (!data.state) {

							alert(data.param);
						} else {
							alert('删除成功');
							window.location = SITE_URL + '/user';
						}
					},
					error: function error() {
						//alert('系统错误')
					},
					dataType: 'json'
				});
			},
			logout: function logout() {
				window.location = SITE_URL + '/passport/logout';
			}
		}
	});

/***/ },
/* 1 */
/***/ function(module, exports) {

	"use strict";

	Object.defineProperty(exports, "__esModule", {
		value: true
	});

	exports.default = function (components) {

		var res = {};
		for (var i = 0; i < components.length; i++) {
			res[components[i]] = Bone[components[i]];
		}
		return res;
	};

/***/ }
/******/ ]);