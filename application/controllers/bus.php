<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bus extends CI_Controller {
	function __construct(){
 		parent::__construct();
 		$this->load->model('user_model');
 
	}

	public function index()
	{	
		$this->load->model('bus_model', '', true);
		$res = $this->bus_model->get_all();
		$data  = array('bus' => $res);
		$this->load->view('carlist', $data);
	}
 
	public function add()
	{
		 
		$this->load->view('caradd');
	}

	public function edit()
	{
		$cid = $this->input->get('cid');
		$this->load->model('category_model', '', true);
		$res = $this->category_model->get_by_id($cid);

		//echo json_encode(array('state' => $res, 'param' => 'fds'));

		$data  = array('category' => $res);
		$this->load->view('category/edit', $data);
	}

	public function update()
	{
		$data = array(
			 
			'uid' => $this->session->userdata('user_id'),
			'name' => $this->input->post('name'),
			'content2' => $this->input->post('content'),
			'type' => $this->input->post('type')
		);
		$this->load->model('category_model', '', true);
		$res = $this->category_model->update_by_id($this->input->post('cid'), $data);
	
		if($res){
			echo json_encode(array('state' => true, 'param' => ''));
		} else {
			echo json_encode(array('state' => false, 'param' => ''));
		}
	}

	public function delete(){
		$this->load->model('bus_model', '', true);
		$res = $this->bus_model->delete_by_id($this->input->get('id'));
		if($res){
			echo json_encode(array('state' => true, 'param' => ''));
		} else {
			echo json_encode(array('state' => false, 'param' => ''));
		}
	
	}


	public function save()
	{
		$data = array(
			'bname' => $this->input->post('bname'),
			'bnumber' => $this->input->post('bnumber'),
			'bbrand' => $this->input->post('bbrand'),
			'state' => $this->input->post('state'),
			'bbrand' => $this->input->post('bbrand'),
			'a' => $this->input->post('a'),
			'b' => $this->input->post('b'),
			'c' => $this->input->post('c'),
			'd' => $this->input->post('d'),
			'e' => $this->input->post('e'),
			'f' => $this->input->post('f'),
			'g' => $this->input->post('g'),
			'h' => $this->input->post('h') 
		);
		$this->load->model('bus_model', '', true);
		$res = $this->bus_model->add($data);
	
		if($res){
			echo json_encode(array('state' => true, 'param' => ''));
		} else {
			echo json_encode(array('state' => false, 'param' => ''));
		}
	}


	public function get_all()
	{

		$type = $this->input->get('type');
		$uid = $this->session->userdata('user_id');



		$this->load->model('category_model', '', true);

		$res = $this->category_model->get_all($type, $uid);
		if($res){
			echo json_encode($res);
		} else {
			echo json_encode(array('state' => false, 'param' => ''));
		}
	}
}
