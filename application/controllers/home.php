<?php
/**
 * user 控制器
 * @authors leedow (644743991@qq.com)
 * @website http://www.leedow.com
 * @date    2014-07-07 19:27:02
 * @version 
 */
if (!defined('BASEPATH'))
	exit('No direct script access allowed');
class Home extends CI_Controller {
	function __construct(){
 		parent::__construct();
 		$this->load->model('user_model');
 		$this->load->helper('url');
	}

	public function index(){
 		$this->load->view('home');
	}
 

	//保存支出
 	public function save_payment()
	{
		$data = array(
			'uid' => $this->session->userdata('user_id'),
			'content' => $this->input->get('content'),
			'count' => $this->input->get('count'),
			'cid' => $this->input->get('cid'),
			'time' => $this->input->get('time')
		);
		$this->load->model('payment_model', '', true);
		$res = $this->payment_model->add($data);
	
		if($res){
			echo json_encode(array('state' => true, 'param' => ''));
		} else {
			echo json_encode(array('state' => false, 'param' => ''));
		}
	}

	//保存收入
 	public function save_income()
	{
		$data = array(
			'uid' => $this->session->userdata('user_id'),
			'content' => $this->input->get('content'),
			'count' => $this->input->get('count'),
			'cid' => $this->input->get('cid'),
			'time' => $this->input->get('time')
		);
		$this->load->model('income_model', '', true);
		$res = $this->income_model->add($data);
	
		if($res){
			echo json_encode(array('state' => true, 'param' => ''));
		} else {
			echo json_encode(array('state' => false, 'param' => ''));
		}
	}

	//根据类别查询收入,类别为0时查询所有
 	public function get_income()
	{

		$cid = $this->input->get('cid');
		$limit = $this->input->get('limit');
		$offset = $this->input->get('offset');
		$uid = $this->session->userdata('user_id');
		$uid = $this->session->userdata('user_id');
		$start = $this->input->get('start')?$this->input->get('start'):0;
		$end = $this->input->get('end')?$this->input->get('end'):999999999999;

		$this->load->model('income_model', '', true);

		if($cid == 0){
			$res = $this->income_model->get_all($limit, $offset, $uid, $start, $end);
		} else {
			$res = $this->income_model->get_by_cid($cid, $limit, $offset, $uid, $start, $end);
		}
		if($res){
			echo json_encode($res);
		} else {
			echo json_encode(array('state' => false, 'param' => ''));
		}
	}

	//根据类别查询支出,类别为0时查询所有
 	public function get_payment()
	{

		$cid = $this->input->get('cid');
		$limit = $this->input->get('limit');
		$offset = $this->input->get('offset');
		$uid = $this->session->userdata('user_id');
		$start = $this->input->get('start')?$this->input->get('start'):0;
		$end = $this->input->get('end')?$this->input->get('end'):9999999999999;

		$this->load->model('payment_model', '', true);

		if($cid == 0){
			$res = $this->payment_model->get_all($limit, $offset, $uid, $start, $end);
		} else {
			$res = $this->payment_model->get_by_cid($cid, $limit, $offset, $uid, $start, $end);
		}
		if($res){
			echo json_encode($res);
		} else {
			echo json_encode(array('state' => false, 'param' => ''));
		}
	}



	//查询个人统计
 	public function get_data()
	{
		
		
		$uid = $this->session->userdata('user_id');
		$this->load->model('payment_model', '', true);
		$this->load->model('income_model', '', true);
		$start = $this->input->get('start')?$this->input->get('start'):0;
		$end = $this->input->get('end')?$this->input->get('end'):9999999999;


		//统计支出
		$payment = $this->payment_model->get_all(999999999, 0, $uid, $start, $end);
		$payment_num = 0;

		for($n=0; $n<count($payment); $n++){
			$payment_num += $payment[$n]->count;

		}


		//统计收入
		$income = $this->income_model->get_all(9999999999, 0, $uid, $start, $end);
		$income_num = 0;

		for($n=0; $n<count($income); $n++){
			$income_num += $income[$n]->count;
		}

		echo json_encode(array('income' => $income_num, 'payment' => $payment_num));
		
	}
	
	 
	 
}	

?>