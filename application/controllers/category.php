<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends CI_Controller {


	public function index()
	{	
		

		$this->load->view('category/home');
	}
 
	public function add()
	{
		 
		$this->load->view('category/add');
	}

	public function edit()
	{
		$cid = $this->input->get('cid');
		$this->load->model('category_model', '', true);
		$res = $this->category_model->get_by_id($cid);

		//echo json_encode(array('state' => $res, 'param' => 'fds'));

		$data  = array('category' => $res);
		$this->load->view('category/edit', $data);
	}

	public function update()
	{
		$data = array(
			 
			'uid' => $this->session->userdata('user_id'),
			'name' => $this->input->post('name'),
			'content2' => $this->input->post('content'),
			'type' => $this->input->post('type')
		);
		$this->load->model('category_model', '', true);
		$res = $this->category_model->update_by_id($this->input->post('cid'), $data);
	
		if($res){
			echo json_encode(array('state' => true, 'param' => ''));
		} else {
			echo json_encode(array('state' => false, 'param' => ''));
		}
	}

	public function delete(){
		$this->load->model('category_model', '', true);
		$res = $this->category_model->delete_by_id($this->input->get('cid'));
		if($res){
			echo json_encode(array('state' => true, 'param' => ''));
		} else {
			echo json_encode(array('state' => false, 'param' => ''));
		}
	
	}


	public function save()
	{
		$data = array(
			'uid' => $this->session->userdata('user_id'),
			'name' => $this->input->post('name'),
			'content2' => $this->input->post('content'),
			'type' => $this->input->post('type')
		);
		$this->load->model('category_model', '', true);
		$res = $this->category_model->add($data);
	
		if($res){
			echo json_encode(array('state' => true, 'param' => ''));
		} else {
			echo json_encode(array('state' => false, 'param' => ''));
		}
	}


	public function get_all()
	{

		$type = $this->input->get('type');
		$uid = $this->session->userdata('user_id');



		$this->load->model('category_model', '', true);

		$res = $this->category_model->get_all($type, $uid);
		if($res){
			echo json_encode($res);
		} else {
			echo json_encode(array('state' => false, 'param' => ''));
		}
	}
}
