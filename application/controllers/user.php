<?php
/**
 * user 控制器
 * @authors leedow (644743991@qq.com)
 * @website http://www.leedow.com
 * @date    2014-07-07 19:27:02
 * @version 
 */
if (!defined('BASEPATH'))
	exit('No direct script access allowed');
class User extends CI_Controller {
	function __construct(){
 		parent::__construct();
 		$this->load->model('user_model');
 		$this->load->helper('url');
	}

	public function index(){
 		$this->load->view('passport/login');
	}
 

 	public function getall(){
 		  
		$res = $this->user_model->get_all();
		if($res){
			echo json_encode($res);
		} else {
			echo json_encode(array('state' => false, 'param' => ''));
		}
	}

	public function edit()
	{
		$uid = $this->input->get('uid');
 
		$res = $this->user_model->get_by_id($uid);

		//echo json_encode(array('state' => $res, 'param' => 'fds'));

		$data  = array('user' => $res);
		$this->load->view('user/edit', $data);
	}

	public function update()
	{
		$data = array(
			'email'	 	=> $this->input->post('email'), 
			'password'	=> md5($this->input->post('password')),
			'name'		=> $this->input->post('name'),
			'role'	=> $this->input->post('role')
		);
	 
		$res = $this->user_model->updating($this->input->post('uid'), $data);
	
		if($res){
			echo json_encode(array('state' => true, 'param' => ''));
		} else {
			echo json_encode(array('state' => false, 'param' => ''));
		}
	}

	public function delete(){
		 
		$res = $this->user_model->delete_by_id($this->input->get('uid'));
		if($res){
			echo json_encode(array('state' => true, 'param' => ''));
		} else {
			echo json_encode(array('state' => false, 'param' => ''));
		}
	
	}

	 

	//注册视图
	public function register(){

		$this->load->view('passport/register');
	}

	//ajax注册接口
	public function registing(){
		 
		$user = array(
			'email'	 	=> $this->input->post('email'), 
			'password'	=> md5($this->input->post('password')),
			'name'		=> $this->input->post('name'),
			'role'	=> $this->input->post('role')
		);

		if ($this->user_model->registing($user)) {
			if ($this->user_model->init($this->input->post('email'))) {
				$this->user_model->login();
			}
			echo json_encode(array('state' => true, 'param' => '' ));
		} else {
			echo json_encode(array('state' => false, 'param' => $this->user_model->get_error()));
		}
	}
	
	 

	//登录视图
	public function login(){
		$this->load->view('passport/login');
	}

	 
	 
	//登录接口
	public function logining(){
		$psd = $this->input->post('password');
		$email = $this->input->post('email');
		if(!$this->user_model->init($email)){
			$data['notice_info'] = $this->user_model->get_error();
			echo json_encode(array('state' => false,'param' => $this->user_model->get_error() ));
		} else {
			if($this->user_model->check_psd($psd)){
				$this->user_model->login();
				$data['notice_info'] = '验证通过';
				$data['redirect_url'] = site_url('home');
				//$this->load->view('notice', $data);
				echo json_encode(array('code' => true, 'msg' => '' ));
			} else {
				$data['notice_info'] = $this->user_model->get_error();
				$data['redirect_url'] = $_SERVER['HTTP_REFERER'];
				echo json_encode(array('code' => false, 'msg' => $this->user_model->get_error() ));
			}
		}
	}

	//注销
	public function unlogining(){
		$this->user_model->unlogin();
		redirect(site_url());
	}

	/* @content 更新信息接口
	 * @param 
	 * @return 	json[param]==00: 没有权限更新 	
	 *			json[param]==01：home_url已经存在
	 *			json[param]==02：更新数据失败 
	 */
	public function updating(){
		if ($this->input->post('user_id') != $this->session->userdata('user_id')) {
			echo json_encode(array('state' => false, 'param' => '00'));
			return ;
		}
		if (!$this->user_model->check_home_url($this->input->post('home_url')) &&
			$this->input->post('home_url') != $this->session->userdata('user_home_url')
			) {
			echo json_encode(array('state' => false, 'param' => '01'));
			return ;
		}
		$data = array(
					'name' 		=> $this->input->post('name'), 
					'content'	=> $this->input->post('content'),
					'avatar_url'=> $this->input->post('avatar_url'),
					'home_url'	=> $this->input->post('home_url')
				);
		if($this->user_model->updating($this->session->userdata('user_id'), $data)){
			$this->user_model->update_session();
			echo json_encode(array('state' => true, 'param' => ''));
		} else {
			echo json_encode(array('state' => false, 'param' => '02'));
		}
	}

	 
}	

?>