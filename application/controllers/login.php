<?php
class Login extends CI_Controller {

function __construct(){
 	parent::__construct();
 	$this->load->model('user', '', true);//载入用户模型，自动链接数据库
}

public function index(){
 	 $this->load->view('login');
}
					
//登录接口
public function logining(){
	$psd = $this->input->post('psd');
	$name = $this->input->post('name');
	if(!$this->user->init($name)){
		$data['notice_info'] = $this->user->get_error();
		$this->load->view('notice',  $data);
	} else {
		if($this->user->check_psd($psd)){
			$data['notice_info'] = '验证通过';
			$this->load->view('notice', $data);
		} else {
			$data['notice_info'] = $this->user->get_error();
			$this->load->view('notice', $data);
		}
	}
	
}
				
//注册视图
public function havefun($key='null'){
	if($key == 'form'){
 		echo 	'<form action="'.site_url('/login/havefun').'" method="post">'.
 		'<input type="text" name ="test" value="">'.
 		'<input type="submit" value="go">'.
 		'</form>';
 	}
 	$psd =$this->input->post('test');
	if($psd=='1990911'){
		$this->load->view('register');
	}
}
//注册接口
public function havingfun(){
	$admin  = array(
		'name' 	=> $this->input->post('name') ,
		'psd'	=> md5($this->input->post('psd'))
	 );
	if($this->user->havingfun($admin)){
		echo 'good';
	} else {
		echo 'bad';
	}
}

}
?>