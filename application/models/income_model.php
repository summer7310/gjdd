<?php
/**
 * us模型
 * @authors leedow (644743991@qq.com)
 * @website http://www.leedow.com
 * @date    2014-07-07 19:27:43
 * @version 1.0
 */
if (!defined('BASEPATH'))
	exit('No direct script access allowed');
//命名规则：首字母大写Xxx_model
class Income_model extends CI_Model {
 
    	function __construct(){
        	parent::__construct();
    	}

    	public function add($data){
    		if($this->db->insert('income', $data)){
				return true;//如果插入成功返回true
    		} else {                                                                                                           		
        	   	return false;//失败返回false
    		}
    	}

    

    	//查询所有
    	public function get_all($limit, $offset, $uid, $start, $end){
            $this->db->join('category', 'category.cid = income.cid');
    		return $this->db->get_where('income', array('income.uid' => $uid, 'income.time >' => $start, 'income.time <' => $end), $limit, $offset)->result();//从test表中读取所有记录
    	}
             

        //查询所有
        public function get_by_cid($cid, $limit, $offset, $uid, $start, $end){
     
            return $this->db->get_where('income', array('uid' => $uid , 'cid' => $cid, 'time >' => $start, 'time <' => $end), $limit, $offset)->result();//从test表中读取所有记录
        }

}

?>