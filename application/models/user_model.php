<?php
/**
 * user模型
 * @authors leedow (644743991@qq.com)
 * @website http://www.leedow.com
 * @date    2014-07-07 19:27:43
 * @version 1.0
 */
if (!defined('BASEPATH'))
	exit('No direct script access allowed');
class User_model extends CI_Model {
 
	private $user_info;//对象个人信息
            	private $error_info;

    	function __construct(){
        		parent::__construct();
    	}

            	//错误信息
            	public function get_error(){
                        return $this->error_info;
            	}

            	public function set_error($info){
                        $this->error_info = $info;
            	}	

            	//初始化成员变量,验证登录时必须调用
            	public function init($email){
                        	if($email != NULL){
                                    	$sql = 'SELECT * FROM users WHERE email ="' . $email.'"';
                                    	$res = $this->db->query($sql);
                                    	$user = $res->result();
                                    	if($user == NULL){
                                                $this->set_error('用户邮箱不存在');
                                                return false;
                                    	}
                                    	$this->user_info = $user[0];
                                    	return true;  
                        	} 
                        	$this->set_error('用户名邮箱不能为空');
                        	return false;                    
            	}
                                            
           	 //检查密码，需init
            	public function check_psd($password){
                        	if(md5($password) == $this->user_info->password){
                        	            return true;
                      	}
                	$this->set_error('密码错误');
                   	return false;
        	}


    	//查找用户名是否存在,需init
    	public function check_exist($name){
    		return false;
    		//预留
    	}

        //查找邮箱是否存在
        public function check_email($email){
            $this->db->where('email', $email);
            //$this->db->from('users')
            $res = $this->db->get('users')->row();
            //$res = $this->db->get_where('users', array('email' => $email ),0,0)->result();
            if ($res == null) {
                return false;
            } else {
                return true;
            }
        }

    	//注册
    	public function registing($user){
            if ($this->check_email($user['email'])) {
                $this->set_error('邮箱已被注册');
                return false;
            }
    		if(isset($user) && $user['email'] != NULL && $user['password'] != NULL){
    			if($this->db->insert('users', $user)){
    				return true;
    			} else {
                    $this->set_error('插入数据发生错误');
    				return false;
    			}
    		} else {
                $this->set_error('信息不完整');
    			return false;
    		}
    	}
        //第三方无邮箱注册
        public function registing2($user){
        
            if($this->db->insert('users', $user)){
                return true;
            } else {
                $this->set_error('插入数据发生错误');
                return false;
            }
           
        }

    	//登录接口
    	public function login(){

    		$user = array(
    			'user_id' 	=> $this->user_info->id,
    			'user_name' 	=> $this->user_info->name,
    			'user_email' 	=> $this->user_info->email,
                //'user_relid'    => $this->user_info->rel_id,
               // 'user_home_url'	=> $this->user_info->home_url,
                //'user_level'	=> $this->user_info->level
    		 );
    		$this->session->set_userdata($user);
            //$this->session->set_userdata('duoshuo_token', '');
    	}

    	//注销登录
    	public function unlogin(){
    		$user = array(
    			'user_id' 	=> '',
    			'user_name' 	=> '',
    			'user_email' 	=> '',
                'user_relid'    => '',
                'user_home_url'	=> '',
                'user_level'	=> ''
    		 );
    		$this->session->unset_userdata($user);
    	}

    	//检测登录	,已经登陆的话写入多说的COOKIE识别信息	
    	public function iflogin(){
    		$user = $this->session->userdata('user_id');
    		if (isset($user) && $user != null) {
                //如果登陆写入多说的cookie
                $this->load->helper('jwt');
                $token = array(
                    "short_name"    => 'cloudlist',
                    "user_key"      => $this->session->userdata('user_id'),
                    "name"          => $this->session->userdata('user_name')
                );
                $duoshuoToken = JWT::encode($token, '1f79177b7f81d7a364c316f28c8193cc');
                setcookie('duoshuo_token', $duoshuoToken);
    			return true;
    		} else {
                //setcookie('duoshuo_token');
                setcookie("duoshuo_token","OKadmin",time()-1);
    			return false;
    		}
    	}

    	/* @content 更新session,一般在用户设置后调用
    	 * @param
    	 * @return boolean
    	 */
    	public function update_session(){
    		$user = $this->get_by_id($this->session->userdata('user_id'));
    		$user = array(
    			'user_id' 	=> $user->id,
    			'user_name' 	=> $user->name,
    			'user_email' 	=> $user->email,
                'user_relid'    => $user->rel_id,
                'user_home_url'	=> $user->home_url,
                'user_level'	=> $user->level
    		 );
    		$this->session->set_userdata($user);

    	}

         //按照ID更新
        public function delete_by_id($uid){
            $this->db->where('id', $uid);
            return $this->db->delete('users');//->first_row();//从test表中读取所有记录
        }


        /* @content 如果已经存在多说账户则登陆，否则返回false
         */
        public function handle_duoshuo($duoshuo){
            $res = $this->get_by_relid($duoshuo['user_id'], 1);
            if ($res == null)  {
                return false;
            } else {
                $user = array(
                    'user_id'   => $res[0]->id,
                    'user_name'     => $res[0]->name,
                    'user_email'    => $res[0]->email,
                    'user_relid'    => $res[0]->rel_id,
                    'user_home_url'	=> $res[0]->home_url,
                    'user_level'	=> $res[0]->level
                 );
                $this->session->set_userdata($user);
                $this->iflogin();

                return true;
            }
        }
        //第三方用户登陆检测
        //检测第三方登陆的rel_id是否存在,$service_type为保留参数
        public function get_by_relid($rel_id, $service_type=null){
            return $this->db->get_where('users', array('rel_id' => $rel_id, 'service_type' => $service_type))->result();
        }

        /* @content 根据home_url获取个人信息
         * @param int $home_url
         * @return object
         */
        public function get_by_hurl($home_url){
        	return $this->db->get_where('users', array('home_url' => $home_url))->row();
        }

        /* @content 根据id查询
         * @param int $uid 用户主键
         * @return user object
         */
        public function get_by_id($uid){
        	return $this->db->get_where('users', array('id' => $uid))->row();
        }

         public function get_all(){
            return $this->db->get('users')->result();
        }

        /* @content 检查home_url是否重复
         * @param string @home_url
         * @return boolean
         */
        public function check_home_url($home_url){
        	$res = $this->db->get_where('users', array('home_url' => $home_url))->row();
        	if ($res == null) {
        		return true;
        	} else {
        		return false;
        	}
        }

        /* @content 根据ID更新用户信息
         * @param int $id
         * @param object $user
         * @return boolean
         */
        public function updating($id, $user){
        	$this->db->where('id' , $id);
        	return $this->db->update('users', $user);
        }

        /* @content 根据用户ID查询个人信息以及最新文章
         * @param int uid
         */
        public function get_info_byid($uid){
        	$this->db->select('users.id, users.name, users.avatar_url, users.home_url, users.weibo_url, users.content,'.
        					'cloud_projects.id as pid, cloud_projects.name as title, cloud_projects.logo_url, cloud_projects.date, cloud_projects.content as abs');
	    	$this->db->from('users');
	    	$this->db->where('users.id',  $uid);
	    	//$this->db->where('state',  1);
	    	$this->db->join('cloud_projects', 'users.id = cloud_projects.belong_user', 'left');
	    	$this->db->order_by("cloud_projects.date", "desc");
	    	$this->db->where('cloud_projects.state',  1);
	    	$this->db->limit(4, 0); 
	    	return $this->db->get()->result();
        }


}

?>