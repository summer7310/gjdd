<?php
/**
 * us模型
 * @authors leedow (644743991@qq.com)
 * @website http://www.leedow.com
 * @date    2014-07-07 19:27:43
 * @version 1.0
 */
if (!defined('BASEPATH'))
	exit('No direct script access allowed');
//命名规则：首字母大写Xxx_model
class Bus_model extends CI_Model {
 
    	function __construct(){
        	parent::__construct();
    	}

    	//插入新的记录
    	//$this->db->insert('test', $data)test指的是数据表的名字，$data是要插入的数据
    	//->相当于JAVA中的.方法 $this->db   java: this.db
    	public function add($data){
    		if($this->db->insert('buses', $data)){
				return true;//如果插入成功返回true
    		} else {                                                                                                           		
        	   	return false;//失败返回false
    		}
    	}





        //查询所有
        public function get_all(){
            //$this->db->join('drivers', 'drivers.bid = buses.bid');
            return $this->db->get('buses')->result();//从test表中读取所有记录
        }

         //按照ID更新
        public function delete_by_id($id){
            $this->db->where('bid', $id);
            return $this->db->delete('buses');//->first_row();//从test表中读取所有记录
        }
             
 

}

?>