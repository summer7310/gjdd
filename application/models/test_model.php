<?php
/**
 * user模型
 * @authors leedow (644743991@qq.com)
 * @website http://www.leedow.com
 * @date    2014-07-07 19:27:43
 * @version 1.0
 */
if (!defined('BASEPATH'))
	exit('No direct script access allowed');
//命名规则：首字母大写Xxx_model
class Test_model extends CI_Model {
 
 		//mvc在codeignier中对应的目录分别是
		//modules:application/models
		//views:application/views
		//controllers:application/controllers
		//如果不是进行其他配置，程序开发一般就动这三个文件夹的的文件就可以了



		/*实现一个基本功能的基本步骤：
		第一步：新建一个module,相当于MVC中的M，主要是进行一些数据库以及业务逻辑的底层操作
				基本方法你可以每次新建一个空的文件，然后复制一个模板进去
				里面的方法可以随意加的，比如添加一个test方法
				这个文件中主要包含两个方法add用于向数据表插入一条新记录
				get_all用于将所有的记录查出来*/

		/*第二部：新建一个controller，相当于mvc中的c，model的作用是处理逻辑业务，
		controller的作用是处理视图（页面）*/
		function test(){
			//新建一个方法
		}


    	function __construct(){
        		parent::__construct();
    	}

    	//插入新的记录
    	//$this->db->insert('test', $data)test指的是数据表的名字，$data是要插入的数据
    	//->相当于JAVA中的.方法 $this->db   java: this.db
    	public function add($data){
    		if($this->db->insert('test', $data)){
				return true;//如果插入成功返回true
    		} else {
        	                                                                                                                          		
        	     return false;//失败返回false
    		}
    	}

    	//查询所有
    	public function get_all(){
    		return $this->db->get('test')->result();//从test表中读取所有记录
    	}
             


}

?>