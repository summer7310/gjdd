<!DOCTYPE html>
<head>
<title>集市宝</title>
<?php $this->load->view('common/source');?>
</head>
<body style="background:#f2f2f2">
	<header class="layout-header">
		<div class="header">
			今日秒杀
			<a class="button-left" href="<?php echo site_url('home/index')?>"><i class="fa fa-chevron-left"></i></a>
			<a class="button-right" href="<?php echo site_url('home/seckill')?>"><i class="fa fa-refresh"></i></a>
		</div>
	</header>
	<div class="notice2"><i class="fa fa-exclamation-circle" ></i> 秒杀时间<?php echo $start.'-'.$end; ?>,加入购物车即可秒杀</div>
	<div class="layout" style="padding-bottom:50px;">
		<ul class="list bortopn">

			<?php if($products == null):?>
 				 
			<div class="none">
				<p class="icon">
					<i class="fa  fa-meh-o"></i>
				</p>
				<p>没有秒杀商品</p>
			</div>
 
			<?php endif; ?>
			<?php 
			foreach ($products as $item):?>
			<li class="list-item list-item-sp seckill" >
				<div class="pic-widget-md">
					<div class="pic">
						<img id="img_product"
				src="http://7xj0kd.com2.z0.glb.qiniucdn.com/<?php echo $item['image'];?>?imageView2/1/w/60/h/60" />
					 	<p>
					 		限购<span><?php echo $item['lim'].$item['unit']; ?></span><br>
					 		剩余<span class="stock"><?php echo $item['stock']; ?></span><?php echo $item['unit']; ?>
					 	</p>
				
					</div>
					<!-- <div class="pic pic2">
						<p>
						限购1件 
					</p>
					</div> -->
					<h3 class="vg-name"><?php echo $item['name'];?> </h3>
					<div class="info">
						<span><?php echo  $item['description'];?></span>
					</div>
					<div class="info" style="padding:0 0 5px 0;">
						<span class="line-through">原价 <?php echo $item['oriprice'];?>元/<?php echo $item['unit'];?></span>	<br/>
						秒杀价 <span style="color:red"><?php echo $item['price'];?></span> 元/<?php echo $item['unit'];?> 
					</div>
					<div class="count-widget" <?php if($item['stock']==0 || $item['amount']>0 || $item['amount2']==1) echo 'data-disable=true';?>>
						<button class="sub"><i class="fa fa-minus"></i></button><input type="tel" value="<?php echo $item['amount']>0?$item['amount']:1;?>" id="num<?php echo $item['id'];?>" ><button class="plus"><i class="fa fa-plus"></i></button>
					</div>
					<div class="toolbar">
					 	<?php if($item['amount2']==1){?>
							<button class="button button-blank-grey button-sm" data-id='' data-amount="<?php echo $item['amount']; ?>">已购买</button>
						<?php }else if($item['amount'] == 0 && $item['stock'] > 0){ ?>
							<button class="button button-blank-warm button-sm add" data-id='<?php echo $item['id']; ?>' data-amount="0" data-lim="<?php echo $item['lim'];?>"><i class="fa fa-cart-plus"></i> 秒杀</button>
						<?php } else if($item['amount'] > 0) { ?>
							<button class="button button-blank-grey button-sm" data-id='' data-amount="<?php echo $item['amount']; ?>"><i class="fa fa-check"></i>购物车</button>
						<?php } else if($item['stock']==0){?>
							<button class="button button-blank-grey button-sm" data-id='' data-amount="<?php echo $item['amount']; ?>">已被抢光</button>
						<?php } ?>
					</div>
				</div>
			</li>
			<?php endforeach;?>
		</ul>

		 
	</div>
 
	 
 
	<div class="row buy">
			<div class="col-6">
				总计 <span class="money" id="total"><?php echo $total ?> </span> 元
			</div>
			<div class="go-car">
				<a href="<?php echo site_url('shoppingcart/index')?>" class="button button-warm button-bg">去购物车结算</a>
			</div>
	</div>
<script type="text/javascript">
$(document).ready(function(){
	leeui.init();
	initPic();
	function initPic(){
		 

		$('.pic').click(function(){
			var img = $(this).find('img').attr('src').split('?')[0];

			leeui.picpre({
				content: '<img src="' + img + '?imageView2/2/w/'+$(window).width()+'/interlace/1" >'
			});
		})
	}
	function switch_button(obj){
		if(parseInt(obj.data('amount'))>0){
			obj.removeClass('button-blank-warm')
			.removeClass('add')
			.addClass('button-blank-grey')
			.html('<i class="fa fa-check"></i> 购物车');
		} else {
			obj.removeClass('button-blank-grey').addClass('button-blank-warm')
			.html('<i class="fa fa-cart-plus"></i> 秒杀');
		}
	}

	$('.add').click(function(){
		var productid = $(this).data('id');
		var _this = $(this);

		if(_this.data('amount')>0){
			return;
		}
		var num = $('#num'+productid).val();
		var lim = $(this).data('lim');
		if(num <= 0){
			alert('数量不能为零');
		}
		if(num > lim){
			alert('数量不能超过限额');
			return;
		}
		$.ajax({
			url : "<?php echo site_url('/shoppingcart/addsectocart'); ?>",
			type : "post",
			//async : false,
			data : {
				'productid' : productid,
				'amount' : num
			},
			dataType : "json",
			success : function(data) {
				if(data.code)
				{
					_this.data('amount', num);
					$('#total').text(data.data['total']);
					_this.parents('.seckill').find('.stock').html(data.data['stock']);
					$('#num'+productid).parent('.count-widget').data('disable', true);
				 	switch_button(_this);
				}else{
					_this.parents('.seckill').find('.stock').html(data.data['stock']);
					alert(data.msg);
				}
			}
		});
	})
	 
})
</script>
</body>