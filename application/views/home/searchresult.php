<!DOCTYPE html>
<head>
<title>集市宝</title>
<?php $this->load->view('common/cmdsource');?>
<script type="text/javascript">
seajs.use("main.home.js?v=" + VERSION_CONF.mainHome);
</script> 
</head>
<body>
	<header class="layout-header">
		<div class="header" style="padding:4px 63px 4px 50px;">
			<div id="search">
				<form action="<?php echo site_url('home/search')?>" method="post">
					<input type="text" name="name" id="name" class="search-input" placeholder="输入商品名称">
					<span class="search-button"><i class="fa fa-search"></i></span>
					<input type="submit" class="search-button" value="" />
				</form>
			</div>
			<a class="button-left" href="<?php echo site_url('home/index')?>" style="padding:7px 6px 0 6px;"><img src="img/logo2.png" width="25"></a>
			<a class="button-right button-right-title" href="<?php echo site_url('user/index')?>"><i class="fa fa-user"></i><span>个人中心</span></a>
		</div>
	</header>
	<div class="layout" style="padding-bottom:50px;">	
			<?php $this->load->view('common/menu'); ?>
			<!-- <div class="divi"></div> -->
			<ul class="list bortopn cate" id="sr">
				<?php $this->load->view('home/list'); ?>
			</ul>
			<div class="divi">

			</div>
			<?php if($products == null):?>
 				 
					<div class="none">
						<p class="icon">
							<i class="fa  fa-meh-o"></i>
						</p>
						<p>没有相关商品</p><p>试试新品预定，我们会及时备货。</p>
						<p><a class="button button-warm" href="<?php echo site_url('user/newproduct')?>">新品预定</a></p>
					</div>
		 
				<?php endif; ?>
			 
	</div>
	<div class="row buy">
			<div class="col-6">
				总计 <span class="money" id="total"><?php echo $total;?> </span> 元
			</div>
			<div class="go-car">
				<a href="<?php echo site_url('shoppingcart/index')?>" class="button button-warm button-md">去购物车结算</a>
			</div>
	</div>

	 
	<?php //$this->load->view('common/sidebar'); ?>

</body>
</html>