<?php 
foreach ($products as $item):?>
<li class="list-item list-item-sp" cat_id="<?php if(isset($item['categoryid'])) echo $item['categoryid'];?>" var_id="<?php if(isset($item['varietyid'])) echo $item['varietyid'];?>">
	<div class="pic-widget-md">
		<div class="pic">
			<img id="img_product"
	src="http://7xj0kd.com2.z0.glb.qiniucdn.com/<?php echo $item['image'];?>?imageView2/2/w/60/interlace/1" />
		</div>
		<h3 class="vg-name">
			<?php echo $item['name'];?>
		</h3>
		<div class="price">
			<span><span class="money"><?php echo $item['price'];?></span> 元/<?php echo $item['unit'];?><?php
				if($item['pricestate']==1){
					echo '<i class="fa  fa-arrow-up up"></i>';
				}elseif($item['pricestate']==-1){
					echo '<i class="fa  fa-arrow-down down"></i>';
				}
			?></span>
		</div>
		<div class="noselect info info-<?php echo $item['id']; ?>" id="info-<?php echo $item['id']; ?>">
			<p>
				<?php
					if($item['type']==1){
						echo "(食堂专用)";
					}
					if($item['start']>0){
						echo "(".$item['start'].$item['unit']."起卖)";
					}
				 	echo $item['description'];
				 ?>
			</p>

			<?php if($item['requirement'] != null){ ?>
			<p class="rq">
				<?php echo $item['requirement']?> <br/>
			<a href="javascript:void(0)" data-id="<?php echo $item['id'];?>" data-name="<?php echo $item['name'];?>" class="requirement"><i class="fa fa-edit"></i>修改</a>
			</p>
			<?php }else{ ?>
			<p class="addrq">
			<a href="javascript:void(0)" data-id="<?php echo $item['id'];?>" data-name="<?php echo $item['name'];?>" class="requirement"><i class="fa fa-edit"></i> 添加备注</a>
			<p>
			<?php } ?>
		</div>
		<div class="count-widget">
			<button class="sub"><i class="fa fa-minus"></i></button><input type="tel" value="<?php echo $item['amount']>0?$item['amount']:$item['start'];?>" class="num<?php echo $item['id'];?>" ><button class="plus"><i class="fa fa-plus"></i></button>
		</div>
		<div class="toolbar">
			<?php if($item['short']==0){ ?>
				<button class="button button-blank-warm button-sm add" data-id='<?php echo $item['id'];?>' data-isstock="<?php echo $item['isstock']; ?>" data-stock="<?php echo $item['stock']; ?>"><i class="fa fa-cart-plus"></i> 购买</button>
			<?php }else{?>
				<button class="button button-blank-grey button-sm">补货中</button>
			<?php }?>
		</div>
	</div>
</li>

<?php endforeach;?>