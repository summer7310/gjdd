<!DOCTYPE html>
<html>
<head>
<title>首 页</title>
<?php $this->load->view('common/include');?>
<style>
	.bo-checked-addon:before {
  position: absolute;
  right: -8px;
  top: -3.2px;
  font-family: iconfont;
  content: '\e628';
  font-size: 16px;
  color: #999;
}
</style>
</head>
<body id="app" v-cloak>
 	<header-layout title='Bone Vue' type="tran">
		<header-dock align='left'>
			<a v-link=""><Icon type="sort"></Icon></a>
		</header-dock>
		<span style="font-size:24px;">家庭理财系统</span>
		<header-dock align='right'>
			<a href=""><Icon type="people"></Icon></a>
		</header-dock>
	</header-layout>
	<layout-main top="none">
		<div class="bo-banner">
			<img src="banner.jpg" alt="">
		</div>
		 
		<layout-box  p="1111">
			<div class="bo-row bo-ovf " data-id="{{item.id}}">
				<div class="bo-col-4">
				 	<div class="info-item">
				 		<h3 class="name">本月总收入</h3>
				 		<p class="content">
				 			{{data.income}} 元
				 		</p>
				 	</div>
				 </div>	
				<div class="bo-col-4">
				 	<div class="info-item">
				 		<h3 class="name">本月总支出</h3>
				 		<p class="content"><span class="price">{{data.payment}}</span> 元</p>
				 	</div>
				 </div>
				<div class="bo-col-4">
				 	<div class="info-item">
				 		<h3 class="name">本月收益</h3>
				 		<p class="content"><span class="price">{{data.income-data.payment}}</span> 元</p>
				 	</div>
				 </div>	  
			 </div>
		</layout-box>

		<layout-box m="1000" p="1111">
			<div class="bo-tabmenu-box">
				<menu-square type="add" href="<?php echo site_url().'/income/add' ?>" color="#FF3300" title="新增收入"></menu-square>
				<menu-square type="cart" href="<?php echo site_url().'/payment/add' ?>" title="新增支出"></menu-square>
			</div>
		</layout-box>	


		<layout-box m="1000">
			<tabs-vertical>
			<tabs-item-vertical arrow=true iconcss=true icon='sort'>
					<a href="<?php echo site_url().'/user' ?>">用户管理</a>
				</tabs-item-vertical>
				<tabs-item-vertical arrow=true iconcss=true icon='sort'>
					<a href="<?php echo site_url().'/category' ?>">分类管理</a>
				</tabs-item-vertical>
				<tabs-item-vertical arrow=true iconcss=true icon='add'>
					<a href="<?php echo site_url().'/income' ?>" >收入管理</a>
				</tabs-item-vertical>
				<tabs-item-vertical arrow=true iconcss=true icon='cart'>
					<a href="<?php echo site_url().'/payment' ?>">支出管理</a>
				</tabs-item-vertical>
			</tabs-vertical>
		</layout-box>
	</layout-main>
	<bottombar></bottombar>
 
	<script src="js/index.build.js?v=1.1"></script>
</body>
</html>
