<!DOCTYPE html>
<head>
<title>集市宝</title>
<?php $this->load->view('common/cmdsource');?>
<script type="text/javascript">
seajs.use("main.home.js?v=" + VERSION_CONF.mainHome);
</script>
</head>
<body>
	<header class="layout-header">
		<div class="header" style="padding:4px 63px 4px 50px;">
			<div id="search">
				<form action="<?php echo site_url('home/search')?>" method="post">
					<input type="text" name="name" id="name" class="search-input" placeholder="输入商品名称">
					<span class="search-button"><i class="fa fa-search"></i></span>
					<input type="submit" class="search-button" value="" />
				</form>
			</div>
			<a class="button-left" href="<?php echo site_url('home/index')?>" style="padding:7px 6px 0 6px;"><img src="img/logo2.png" width="25"></a>
			<a class="button-right button-right-title" href="<?php echo site_url('user/index')?>"><i class="fa fa-user"></i><span>个人中心</span></a>
		</div>
	</header>
	<div class="layout" style="padding-bottom:50px;">
			<?php $this->load->view('common/menu'); ?>
			<ul class="list bortopn">
				<?php foreach ($products as $item):?>
				<li class="list-item list-item-sp">
					<div class="pic-widget-md">
						<div class="pic">
							<img id="img_product"
					src="http://7xj0kd.com2.z0.glb.qiniucdn.com/<?php echo $item['image'];?>?imageView2/2/w/60/interlace/1" />
						</div>
						<h3 class="vg-name"><?php echo $item['name'];?> </h3>
						<div class="price">
							<span> <span class="money"><?php echo $item['price'];?></span> 元/<?php echo $item['unit'];?></span>
						</div>
						<div class="info">
							<span><?php echo $item['description'];?></span>
						</div>
						<div class="info">
							仅剩 <span style="color:red"><?php echo $item['stock'];?></span> <?php echo $item['unit'];?>
						</div>
						<div class="count-widget">
							<button class="sub"><i class="fa fa-minus"></i></button><input type="tel" value="<?php echo $item['amount'];?>" class="num<?php echo $item['id'];?>" onclick="this.select()"><button class="plus"><i class="fa fa-plus"></i></button>
						</div>
						<div class="toolbar">
							<button class="button button-blank-warm button-sm add" data-id='<?php echo $item['id'];?>'><i class="fa fa-cart-plus"></i> 购买</button>
						</div>
					</div>
				</li>
				<?php endforeach;?>
			</ul>
			<!-- <div class="divi"></div> -->
			<!-- <div class="list-item" style="padding-top:0;">
				<p style="color:#999;">没有想买的商品？试试新品预定</p>
				<a class="button button-blank button-block" href="<?php echo site_url('user/newproduct')?>"><i class="fa  fa-bookmark"></i> 新品预定</a>
			</div> -->
	</div>
	<div class="row buy">
			<div>
				<input type="hidden" name="sale" id="sale" value="1"> 
			</div>
			<div class="col-6">
				总计 <span class="money" id="total"><?php echo $total;?> </span> 元
			</div>
			<div class="go-car">
				<a href="<?php echo site_url('shoppingcart/index')?>" class="button button-warm button-md">去购物车结算</a>
			</div>
	</div>

	 
	<?php //$this->load->view('common/sidebar'); ?>
	
</body>