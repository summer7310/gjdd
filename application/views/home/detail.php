<!DOCTYPE html>
<html>
<head>
<title>商品详情</title>
<?php $this->load->view('common/include');?>
<style>
	.bo-checked-addon:before {
  position: absolute;
  right: -8px;
  top: -3.2px;
  font-family: iconfont;
  content: '\e628';
  font-size: 16px;
  color: #999;
}
</style>
</head>
<body id="app" v-cloak>
	<header-layout title='' pos="absolute" type="tran">
		<header-dock align='left'>
			<a href="<?php echo site_url('/home'); ?>"><Icon type="back"></Icon></a>
		</header-dock>
		商品详情
		<header-dock align='right'>
			<a href="<?php echo site_url('/user'); ?>"><icon type="people"></icon></a>
		</header-dock>
	</header-layout>
	<layout-main top="none">
		<div class="bo-banner">
			<img src="http://7xj0kd.com2.z0.glb.qiniucdn.com/<?php echo $product['image']; ?>?imageView2/1/w/120/h/80" alt="">
		</div>
		<layout-box p="1111">
		
			<h3><?php echo $product['name']; ?>(星期{{week[currentDay]}})</h3>
			<grid-row p="0000">
				<form-stars val="3" size="sm" disable=true></form-stars>
				<div class="bo-clear"></div>
			</grid-row>
			<p style="padding:0;margin:0;"><?php echo $product['description']; ?></p>
			<grid-row p="1000">
				<div class="product-dock">
					<div class="price">
						￥<span><?php echo $product['price']; ?></span> /元
					</div>
					<div class="mount">
						<form-check @check-click="buy" :checked=<?php if($product['amount']>0){echo 'true';}else {echo 'false';} ?> v-ref:check></form-check>						
					</div>		
				</div>
				<div class="bo-clear"></div>
			</grid-row>
			 
		</layout-box>
		<h3 class="bo-box-title">商品成分</h3>
		<layout-box p="0110">
			<grid-row>
			<?php foreach ($materials as $item): ?>
				<grid-col size="4" p="1001">
					<div class="nutri">
						<div class="pic">
							<img src="http://7xj0kd.com2.z0.glb.qiniucdn.com/<?php echo $item['image']; ?>?imageView2/1/w/60/h/60" alt="">
						</div>
						<h3><?php echo $item['name']; ?></h3>
						<p><?php echo $item['amount']; ?><?php echo $item['unit']; ?></p>
					</div>
				</grid-col>
			<?php endforeach; ?>	 				 
			</grid-row>			
		</layout-box>
		<h3 class="bo-box-title">商品评价</h3>
		<list @list-scroll="loadingComments" v-ref:comments>
			<list-item v-for="item in comments">
				<layout-box p="1111">
					<div class="bo-comment">
				 		<h3 class="name">{{item.obj.name}}</h3>
				 		<div class="info">
							<form-stars size="sm" disable=true :val="item.obj.score"></form-stars>
							{{item.obj.date}} 			
				 		</div>
				 		<p class="content">
				 			{{item.obj.content}}
				 		</p>
				 		<p v-if="item.obj.reply" class="reply"><span class="reply-name">回复：</span>
							{{item.obj.reply}}
				 		</p>
				 	</div>
				</layout-box>
			</list-item>		 
		</list> 
	</layout-main>
	<shopping-dock :total="total" @shoppingdock-go="gotoshp"></shopping-dock>
	<script>
		var PRODUCT = <?php echo json_encode($product);?>;
		var TOTAL= <?php echo $total; ?>
	</script>
	<script src="js/product.build.js"></script>
</body>
</html>
