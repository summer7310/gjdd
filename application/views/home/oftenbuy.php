<!DOCTYPE html>
<head>
<title>集市宝</title>
<?php $this->load->view('common/cmdsource');?>
<script type="text/javascript">
seajs.use("main.oftenbuy.js?v=" + VERSION_CONF.oftenBuy);
</script>
</head>
<body>
	<header class="layout-header">
		<div class="header" style="padding:4px 63px 4px 50px;">
			<div id="search">
				<form action="<?php echo site_url('home/search')?>" method="post">
					<input type="text" name="name" id="name" class="search-input" placeholder="输入商品名称">
					<span class="search-button"><i class="fa fa-search"></i></span>
					<input type="submit" class="search-button" value="" />
				</form>
			</div>
			<a class="button-left" href="<?php echo site_url('home/index')?>" style="padding:7px 6px 0 6px;"><img src="img/logo2.png" width="25"></a>
			<a class="button-right button-right-title" href="<?php echo site_url('user/index')?>"><i class="fa fa-user"></i><span>个人中心</span></a>
		</div>
	</header>
	<div class="layout" style="padding-bottom:50px;">
		
		<?php $this->load->view('common/menu'); ?>
		<div class="cate">
			<?php if($products == null):?>
 				 
					<div class="none">
						<p class="icon">
							<i class="fa  fa-meh-o"></i>
						</p>
						<p>还没有购买过商品</p>
						<p><a class="button button-warm" href="<?php echo site_url('home/index')?>">去挑选商品</a></p>
					</div>
		 
				<?php endif; ?>
			<ul class="list bortopn" id="cate-none-none" page="20" offset="20">
				
				 <?php $this->load->view('home/list'); ?>

			</ul>
			<?php if($products != null):?>
			<div class="list-item" style="padding-top:0;display:none;border-bottom:0;" id="preorder">
				<p style="color:#999;">想要购买更多商品？</p>
				<a class="button button-blank button-block" href="<?php echo site_url('home/index')?>">
					<i class="fa  fa-bookmark"></i> 去主页选购
				</a>
			</div>
			<?php endif; ?>
		</div>
			<!-- <div class="divi"></div> -->
			<!-- <div class="list-item" style="padding-top:0;">
				<p style="color:#999;">没有想买的商品？试试新品预定</p>
				<a class="button button-blank button-block" href="<?php echo site_url('user/newproduct')?>"><i class="fa  fa-bookmark"></i> 新品预定</a>
			</div> -->
	</div>
	<div class="row buy">
			<div class="col-6">
				总计 <span class="money" id="total"><?php echo $total;?> </span> 元
			</div>
			<div class="go-car">
				<a href="<?php echo site_url('shoppingcart/index')?>" class="button button-warm button-md">去购物车结算</a>
			</div>
	</div>

	 <div class="goback" id="gotop">
		<i class="fa  fa-chevron-up"></i>
	</div>
 
	
</body>