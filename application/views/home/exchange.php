<!DOCTYPE html>
<head>
<title>集市宝</title>
<?php $this->load->view('common/source');?>
<style>
	.pic-widget-md{
		min-height: auto;
	}
</style>
</head>
<body style="background:#f2f2f2">
	<header class="layout-header">
		<div class="header">
			积分兑换
			<a class="button-left" href="<?php echo site_url('home/index')?>"><i class="fa fa-chevron-left"></i></a>
			<div class="button-right">
				<span class="money" id="totalscore"><?php echo $score; ?> </span><span style="font-size:12px;">积分</span> 
			</div>
		</div>
	</header> 
	<div class="layout" style="padding-bottom:50px;">
		<?php if($products == null):?>
 				 
			<div class="none">
				<p class="icon">
					<i class="fa  fa-meh-o"></i>
				</p>
				<p>没有可兑换的商品</p>
			</div>
 
		<?php endif; ?>
		<ul class="list bortopn">
			<?php 
			foreach ($products as $item):?>
			<li class="list-item list-item-sp seckill" >
				<div class="pic-widget-md">
					<div class="pic">
						<img id="img_product"
				src="http://7xj0kd.com2.z0.glb.qiniucdn.com/<?php echo $item['image'];?>?imageView2/1/w/60/h/60" />
					 	 
						 <p style="padding-left:0;padding-right:0;">
						 	剩余<span id="stock<?php echo $item['id']; ?>"><?php echo $item['stock']; ?></span><?php echo $item['unit']; ?>

						 </p>
					</div>
			 
					<h3 class="vg-name"><?php echo $item['name'];?></h3>
					
					<div class="info" <?php if($item['description']=='')echo 'style="padding:0"'; ?>><?php echo $item['description'];?></div>
					<div class="info" style="padding:0 0 5px 0;">
						<span class="line-through">原价 <?php echo $item['oriprice'];?> 元/<?php echo $item['unit'];?></span>	<br/>
						现需 <span style="color:red"><?php echo $item['score'];?></span> 积分
						<?php if($item['price']!=0)echo '+ <span style="color:red">'.$item['price'].'</span>元'; ?>/<?php echo $item['unit'];?>
					</div>
					<div class="count-widget">
						<button class="sub"><i class="fa fa-minus"></i></button>
						<input type="tel" value="0" id="num<?php echo $item['id'];?>">
						<button class="plus"><i class="fa fa-plus"></i></button>
					</div>
					<div class="toolbar"> 	 
							<button class="button button-blank-warm button-sm add" data-id='<?php echo $item['id']; ?>' data-amount=""><i class="fa fa-gift"></i> 兑换</button>
				 
					</div>
				</div>
			</li>
	 		<?php endforeach;?>
		</ul>

		 
	</div>
 
	<div class="row buy">
		<div class="col-6">
			总计 <span class="money" id="total"><?php echo $total ?> </span> 元
		</div>
		<div class="go-car">
			<a href="<?php echo site_url('shoppingcart/index')?>" class="button button-warm button-bg">去购物车结算</a>
		</div>
	</div>
  
<script type="text/javascript">
leeui.init();
initPic();
function initPic(){
	$('.pic').click(function(){
		var img = $(this).find('img').attr('src').split('?')[0];
		leeui.picpre({
			content: '<img src="' + img + '?imageView2/2/w/'+$(window).width()+'/interlace/1" >'
		});
	})
}
$(document).ready(function(){
	$('.add').click(function(){
		var productid = $(this).data('id');
		var _this = $(this);

		 
		var num = $('#num'+productid).val();
		 
		if(num <= 0){
			alert('数量不能为零');
			return;
		}
		 
		$.ajax({
			url : "<?php echo site_url('/shoppingcart/addscoretocart'); ?>",
			type : "post",
			//async : false,
			data : {
				'productid' : productid,
				'amount' : num
			},
			dataType : "json",
			success : function(data) {
				if(data.code)
				{
					_this.data('amount', num);
					$('#stock'+productid).text(data.data['stock']);
					$('#total').text(data.data['total']);
					leeui.numbers($('#totalscore'), parseInt(data.data.score));
				 
				}else{
					 
					alert(data.msg);
				}
			}
		});
	})
})
</script>
</body>