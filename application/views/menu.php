<div class="container-fluid p0">
	<nav class="navbar navbar-default header" role="navigation"> 
	    <!-- Brand and toggle get grouped for better mobile display -->
	    <div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">toggle</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="#"><span class="glyphicon glyphicon-map-marker"></span> 城市物流配送路径规划系统 <span class="badge">Beta</span></a>
	    </div>
	    <!-- Collect the nav links, forms, and other content for toggling -->
	    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav navbar-right">
				<li><a href=""><span class="glyphicon glyphicon-user"></span> 管理员，你好！</a></li>
				<li class="dropdown">
				<a href="" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-cog"></span> 操作 <span class="caret"></span></a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="" data-toggle="modal" data-target="#sign">用户注册</a></li>
						<li role="separator" class="divider"></li>
						<li><a href="" data-toggle="modal" data-target="#myModal">用户登录</a></li>
						<li role="separator" class="divider"></li>
						<li><a href="" data-toggle="modal" data-target="#">注销登录</a></li>
					</ul>
				</li>
			</ul>
			<ul class="nav navbar-nav">
				 

				 
			 

				<li class="dropdown">
					<a href="" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-inbox"></span> 公交调度
						<b class="caret"></b>
					</a>
					<ul class="dropdown-menu">
						<li><a href="#/busArrange">排班管理</a></li>
						<li class="divider"></li>
						<li><a href="" data-toggle="modal" data-target="#setOffBus">发车调度</a></li>
						<li role="separator" class="divider"></li>
						<li><a href="" data-toggle="modal" data-target="#BAOCHE">添加包车信息</a></li>
						<li role="separator" class="divider"></li>
						<li><a href="" data-toggle="modal" data-target="#FANGKONG">添加放空调度信息</a></li>
						<li role="separator" class="divider"></li>
						<li><a href="" data-toggle="modal" data-target="#QUJIAN">添加区间调度信息</a></li>
						<li role="separator" class="divider"></li>
						<li><a href="" data-toggle="modal" data-target="#CHELIANG">添加车辆信息</a></li>
						<li role="separator" class="divider"></li>
						<li><a href="" data-toggle="modal" data-target="#XGCL">修改车辆信息</a></li>
						<li role="separator" class="divider"></li>
						<li><a href="" data-toggle="modal" data-target="#FKDD">放空调度</a></li>
						<!-- <li><a href="#/">其他</a></li> -->
					</ul>
				</li> 
			</ul>
			    
	    </div><!-- /.navbar-collapse -->  
	</nav>
</div>
