<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
<base href="<?php echo site_url('')?>"/>
<link rel="stylesheet" href="css/bone.css?v=1.2">
<link rel="stylesheet" href="font/iconfont.css">
<script src="js/zepto.min.js"></script>
<script type="text/javascript">
var SITE_URL = "<?php echo site_url('')?>";
var BASE_URL = "<?php echo base_url('')?>";
</script>
<script src="js/bone.js?v=1.2.4"></script>
<style>
	[v-cloak] { display: none }
</style>