<!DOCTYPE html>
<html>
<head>
<title>添加分类</title>
<?php $this->load->view('common/include');?>
<style>

</style>
</head>
<body id="app" v-cloak>
		<header-layout title='Bone Vue'>
			<header-dock align='left'>
				<a href="<?php echo site_url().'/home' ?>"><Icon type="back"></Icon></a>
			</header-dock>
			类目管理		
			<header-dock align='right'>
				<a href="<?php echo site_url().'/category/add' ?>">添加分类</a>
			</header-dock>	
		</header-layout>
		<layout-main>			
				<layout-box p="1111" v-for="item in type1">
					<div class="bo-row bo-ovf " data-id="{{item.id}}">
						<div class="bo-col-4">
						 	<div class="info-item">
						 		<h3 class="name">名称</h3>
						 		<p class="content">
						 			{{item.name}}
						 		</p>
						 	</div>
						 </div>	
						 <div class="bo-col-3">
						 	<div class="info-item">
						 		<h3 class="name">类别</h3>
						 		<p class="content">
						 			{{type[item.type-1].text}}					 		
						 		</p>
						 	</div>
						 </div>	
						<div class="bo-col-5">
						 	<div class="info-item">
						 		<h3 class="name">备注</h3>
						 		<p class="content">
						 			{{item.content2}}
						 		</p>
						 	</div>
						 </div>							  
					 </div>
					 <div class="bo-row" style="text-align:right">
					 
					 	<a class="bo-btn-blank" href="<?php echo site_url('category/edit'); ?>?cid={{item.cid}}">编 辑</a>
					 	<btn @btn-click="delete(item.cid)" blank=true name="删 除"></btn>
					 </div>
				</layout-box>	
				 
				<layout-box p="1111" v-for="item in type2">
					<div class="bo-row bo-ovf " data-id="{{item.id}}">
						<div class="bo-col-4">
						 	<div class="info-item">
						 		<h3 class="name">名称</h3>
						 		<p class="content">
						 			{{item.name}}
						 		</p>
						 	</div>
						 </div>	
						 <div class="bo-col-3">
						 	<div class="info-item">
						 		<h3 class="name">类别</h3>
						 		<p class="content">
						 			{{type[item.type-1].text}}
						 		
						 		</p>
						 	</div>
						 </div>	
						<div class="bo-col-5">
						 	<div class="info-item">
						 		<h3 class="name">备注</h3>
						 		<p class="content">
						 			{{item.content}}
						 		</p>
						 	</div>
						 </div>
							  
					 </div>
				</layout-box>	
						  	

		</layout-main>
	<script src="js/category.build.js"></script>
</body>
</html>
