<!DOCTYPE html>
<html>
<head>
<title>添加分类</title>
<?php $this->load->view('common/include');?>
<style>

</style>
</head>
<body id="app" v-cloak>
		<header-layout title='Bone Vue'>
			<header-dock align='left'>
				<a href="<?php echo site_url().'/category' ?>"><Icon type="back"></Icon></a>
			</header-dock>
			编辑分类
			
		</header-layout>
		<layout-main>
			
			<form-group type='blank' v-ref:form>
				<layout-box >
					<div style="display:none;">
					<form-input name="cid" required=true place-holder="请输入分类名" label="分 类" val="<?php echo $category->cid; ?>"  size="lg"></form-input>
					</div> 
					<form-input name="name" required=true place-holder="请输入分类名" label="分 类" val="<?php echo $category->name; ?>"  size="lg"></form-input>
					 
					 							 
					<form-input name="content" required=true place-holder="填写备注" val="<?php echo $category->content; ?>" label="备 注" size="lg"></form-input>
					<form-select 
						:data="type" 
						name="type" 
						required=true 
						place-holder="请选择分类类型" 
						label="类型" 
						size="lg"
						 
						></form-select>
				</layout-box>	
				 
			</form-group>
						  	
			<grid-row p='1111'>
				<Btn type="primary" size="lg" block=true @btn-click="update" name="提 交"></Btn>
			</grid-row>
		</layout-main>
	<script src="js/category.build.js"></script>
</body>
</html>
