<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<title>城市物流配送系统 </title>
	<?php include('source.php'); ?>

</head>
<body>
<!-- 导航栏 -->	

 <?php include('menu.php'); ?>
<div class="container" style="padding-top:20px;">
	
	<div class="col-sm-3">
		<div class="list-group">
		  
		  <a href="#" class="list-group-item">车辆列表</a>
		  <a href="#" class="list-group-item">添加车辆</a>
		  <a href="#" class="list-group-item">司机列表</a>
		  <a href="#" class="list-group-item">添加司机</a>
		</div>
	</div>
	<div class="col-sm-9">
		<form class="form-horizontal">
            	<div class="form-group">
				    <label for="inputEmail3" class="col-sm-2 control-label">车号：</label>
				    <div class="col-sm-4">
				      <input type="email" class="form-control" id="inputEmail3" placeholder="">
				    </div>
					<label for="inputEmail3" class="col-sm-2 control-label">号牌号码：</label>
				    <div class="col-sm-4">

						  <input type="email" class="form-control" id="inputEmail3" placeholder="">

				    </div>
			  	</div> 
            	<div class="form-group">
				    <label for="inputEmail3" class="col-sm-2 control-label">车辆型号：</label>
				    <div class="col-sm-4">
				     	 <select class="form-control">
						  <option>11</option>
							<option>11</option>
						</select>
				    </div>
				    <label for="inputEmail3" class="col-sm-2 control-label">有无空调：</label>
				    <div class="col-sm-4">
				     	 <select class="form-control">
						  <option>有</option>
							<option>无</option>
						</select>
				    </div>
			  	</div>			  	 
            	<div class="form-group">
				    <label for="inputEmail3" class="col-sm-2 control-label">机构类型：</label>
				    <div class="col-sm-4">
				       <select class="form-control">
						  <option>全部</option>
							<option>11</option>
						</select>
				    </div>
				    <label for="inputEmail3" class="col-sm-2 control-label">资金来源：</label>
				    <div class="col-sm-4">
				      <select class="form-control">
						  <option>国波</option>
							
						</select>
				    </div>
			  	</div>
            	<div class="form-group">
				    <label for="inputEmail3" class="col-sm-2 control-label">组织机构：</label>
				    <div class="col-sm-4">
				      <select class="form-control">
						  <option>公交总公司</option>
							
						</select>
				    </div>
				    <label for="inputEmail3" class="col-sm-2 control-label">投产日期：</label>
				    <div class="col-sm-4">
			            <div class="input-group date form_datetime" data-date="2015-01-01" data-date-format="yyyy-mm-dd " data-link-field="dtp_input1">
				        <input id="start_time" class="form-control" size="16" type="text" value="" readonly>       
				        <span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span>
				      </div>
				      <input type="hidden" id="dtp_input1" value="" /> 
				    </div>
			  	</div>
            	<div class="form-group">
				    <label for="inputEmail3" class="col-sm-2 control-label">运营方式：</label>
				    <div class="col-sm-4">
				     	<select class="form-control">
						  <option>运营</option>
							
						</select>
				    </div>
				    <label for="inputEmail3" class="col-sm-2 control-label">机动车辆：</label>
				    <div class="col-sm-4">
				     	<input type="radio" name="sex" value="male">是&nbsp&nbsp&nbsp&nbsp
						<input type="radio" name="sex" value="female">否
				    </div>
			  	</div>
			  	<div class="form-group">
				    <label for="inputEmail3" class="col-sm-2 control-label">减少方式：</label>
				    <div class="col-sm-4">
				     	<select class="form-control">
						  <option>运营</option>
							
						</select>
				    </div>
				    <label for="inputEmail3" class="col-sm-2 control-label">车辆状态：</label>
				    <div class="col-sm-4">
				     	<select class="form-control">
						  <option>可用</option>
							
						</select>
				    </div>
			  	</div>
			  	<div class="form-group">
				    <label for="inputEmail3" class="col-sm-2 control-label">备注：</label>
				    <div class="col-sm-4">
				     	<textarea rows="4" cols="80"></textarea>
				    </div>
				    
			  	</div>
			  	<div class="form-group">
				    <label for="inputEmail3" class="col-sm-2 control-label"> </label>
				    <div class="col-sm-4">
				     	<button class="btn-primary">添 加</button>
				    </div>
				    
			  	</div>
				<div class="form-group col-offset-2">
					 
				</div>
			</form>

	</div>
</div>
 
</body>
<script type="text/javascript" src="js/bootstrap/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="js/bootstrap/bootstrap-datetimepicker.zh-CN.js"></script>

</html>