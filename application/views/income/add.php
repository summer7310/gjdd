<!DOCTYPE html>
<html>
<head>
<title>添加分类</title>
<?php $this->load->view('common/include');?>
<style>

</style>
</head>
<body id="app" v-cloak>
		<header-layout title='Bone Vue'>
			<header-dock align='left'>
				<a href=""><Icon type="back"></Icon></a>
			</header-dock>
			添加收入
			
		</header-layout>
		<layout-main>
			
			<form-group type='blank' v-ref:form>
				<layout-box >
					<form-input name="count" required=true place-holder="填写数额" label="数 额"  size="lg"></form-input>
					 
					 							 
					<form-input name="content" required=true place-holder="填写备注" label="备 注" size="lg"></form-input>
					<form-select 
						:data="category" 
						name="cid" 
						required=true 
						place-holder="请选择分类" 
						label="类 目" 
						size="lg"
						 
						></form-select>
				</layout-box>	
				 
			</form-group>
						  	
			<grid-row p='1111'>
				<Btn type="primary" size="lg" block=true @btn-click="submit" name="提 交"></Btn>
			</grid-row>
		</layout-main>
	<script src="js/income.build.js"></script>
</body>
</html>
