<!DOCTYPE html>
<html>
<head>
<title>添加分类</title>
<?php $this->load->view('common/include');?>
<style>

</style>
</head>
<body id="app" v-cloak>
		<header-layout title='Bone Vue'>
			<header-dock align='left'>
				<a href="<?php echo site_url().'/home' ?>"><Icon type="back"></Icon></a>
			</header-dock>
			收入管理	
			<header-dock align='right'>
				<a href="<?php echo site_url().'/income/add' ?>">添加收入</a>
			</header-dock>	
		</header-layout>
		<layout-main>		
				<layout-box>
					<tabs-horizon>
						<tabs-item-horizon current=true size=6><span @click="getdata(1)">本月({{total}}元)</span></tabs-item-horizon>
						<tabs-item-horizon size=6><span @click="getdata(2)">上月</span></tabs-item-horizon>
					</tabs-horizon>
				</layout-box>	
				<layout-box p="1111" v-for="item in data">
					<div class="bo-row bo-ovf " data-id="{{item.id}}">
						<div class="bo-col-3">
						 	<div class="info-item">
						 		<h3 class="name">额度</h3>
						 		<p class="content">
						 			{{item.count}}
						 		</p>
						 	</div>
						 </div>	
						  <div class="bo-col-3">
						 	<div class="info-item">
						 		<h3 class="name">类别</h3>
						 		<p class="content">
						 			{{item.name}}	 		
						 		</p>
						 	</div>
						 </div>	
						 <div class="bo-col-4">
						 	<div class="info-item">
						 		<h3 class="name">备注</h3>
						 		<p class="content">
						 			{{item.content}}	 		
						 		</p>
						 	</div>
						 </div>	
						<div class="bo-col-2">
						 	<div class="info-item">
						 		<h3 class="name">时间</h3>
						 		<p class="content">
						 			{{item.time}}
						 		</p>
						 	</div>
						 </div>
							  
					 </div>
				</layout-box>	
				 
			

		</layout-main>
	<script src="js/income.build.js"></script>
</body>
</html>
