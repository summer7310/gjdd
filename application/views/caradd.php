<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<title>城市物流配送系统 </title>
	<?php include('source.php'); ?>

</head>
<body>
<!-- 导航栏 -->	

 <?php include('menu.php'); ?>
<div class="container" style="width: 100%;padding-top:20px;">
	
	<?php include('side.php'); ?>
	<div class="col-sm-10">
		<div class="form-horizontal" id="form">
            	<div class="form-group">
				    <label for="inputEmail3" class="col-sm-2 control-label">车号：</label>
				    <div class="col-sm-4">
				      <input name="bname" type="email" class="form-control" id="inputEmail3" placeholder="">
				    </div>
					<label for="inputEmail3" class="col-sm-2 control-label">号牌号码：</label>
				    <div class="col-sm-4">

						  <input name="bnumber" type="email" class="form-control" id="inputEmail3" placeholder="">

				    </div>
			  	</div> 
            	<div class="form-group">
				    <label for="inputEmail3" class="col-sm-2 control-label">车辆型号：</label>
				    <div class="col-sm-4">
				     	 <select name="bbrand" class="form-control">
						  <option val="A2015AX">A2015AX</option>
							<option val="B432DA">B432DA</option>
						</select>
				    </div>
				    <label for="inputEmail3" class="col-sm-2 control-label">有无空调：</label>
				    <div class="col-sm-4">
				     	 <select name="a" class="form-control">
						  <option val='有'>有</option>
							<option val="无">无</option>
						</select>
				    </div>
			  	</div>			  	 
            	<div class="form-group">
				    <label for="inputEmail3" class="col-sm-2 control-label">机构类型：</label>
				    <div class="col-sm-4">
				       <input name="b" type="email" class="form-control" id="inputEmail3" placeholder="">

				    </div>
				    <label for="inputEmail3" class="col-sm-2 control-label">资金来源：</label>
				    <div class="col-sm-4">
				      <select name="c" class="form-control">
						  <option val="国拨">国拨</option>
							
						</select>
				    </div>
			  	</div>
            	<div class="form-group">
				    <label for="inputEmail3" class="col-sm-2 control-label">组织机构：</label>
				    <div class="col-sm-4">
				      <select name="d" class="form-control">
						  <option val="公交总公司">公交总公司</option>
							
						</select>
				    </div>
				    <label for="inputEmail3" class="col-sm-2 control-label">投产日期：</label>
				    <div class="col-sm-4">
			            <div class="input-group date form_datetime" data-date="2015-01-01" data-date-format="yyyy-mm-dd " data-link-field="dtp_input1">
				        <input name="e" id="start_time" class="form-control" size="16" type="text" value="" readonly>       
				        <span class="input-group-addon"><span class="glyphicon glyphicon-time"></span></span>
				      </div>
				      <input type="hidden" id="dtp_input1" value="" /> 
				    </div>
			  	</div>
            	<div class="form-group">
				    <label for="inputEmail3" class="col-sm-2 control-label">运营方式：</label>
				    <div class="col-sm-4">
				     	<select name="f" class="form-control">
						  <option val="运营">运营</option>
							
						</select>
				    </div>
				    <label for="inputEmail3" class="col-sm-2 control-label">机动车辆：</label>
				    <div class="col-sm-4">
				     	<input type="radio" name="g" value="是">是&nbsp&nbsp&nbsp&nbsp
						<input type="radio" name="g" value="否">否
				    </div>
			  	</div>
			  	<div class="form-group">
				    <label for="inputEmail3" class="col-sm-2 control-label">减少方式：</label>
				    <div class="col-sm-4">
				     	<select name="h" class="form-control">
						  <option val="运营">运营</option>
							
						</select>
				    </div>
				    <label for="inputEmail3" class="col-sm-2 control-label">车辆状态：</label>
				    <div class="col-sm-4">
				     	<select name="state" class="form-control">
						  <option val="可用">可用</option>
						  <option val="不可用">不可用</option>
							
						</select>
				    </div>
			  	</div>
			  	 
			  	<div class="form-group">
				    <label for="inputEmail3" class="col-sm-2 control-label"> </label>
				    <div class="col-sm-4">
				     	<button class="btn-primary" id="submit">添 加</button>
				    </div>
				    
			  	</div>
				<div class="form-group col-offset-2">
					 
				</div>
			</div>

	</div>
</div>
 
</body>
<script type="text/javascript" src="js/bootstrap/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="js/bootstrap/bootstrap-datetimepicker.zh-CN.js"></script>
<script>
	$('#submit').click(function(){
    if(bone.verify.check('#form')){
      var data = bone.verify.getData();

        $.ajax({
          url: SITE_URL+'/bus/save',
          type: 'post',
          data: data,
          success: function(d){
            if(d.state){
              alert('添加成功')
            } else {
             alert('添加失败')
            }
          },
          error: function(){
           //bone.dialog.init('','发生错误，请重试');
          },
          dataType: 'json'
        })
    }
  })
</script>
</html>