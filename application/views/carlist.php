<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<title>城市物流配送系统 </title>
	<?php include('source.php'); ?>

</head>
<body>
<!-- 导航栏 -->	

 <?php include('menu.php'); ?>
<div class="container" style="width: 100%;padding-top:20px;">
	
	<?php include('side.php'); ?>
	<div class="col-sm-10">
		<table class="table">
	      <caption>车辆列表</caption>
	      <thead>
	        <tr>
	          <th>#</th>
	          <th>车号</th>
	          <th>车牌</th>
	          <th>型号</th>
	          <th>空调</th>
	          <th>机构</th>
	          <th>资金来源</th>
	          <th>组织机构</th>
	          <th>日期</th>
	          <th>运营方式</th>
	          <th>机动车辆</th>
	          <th>减少方式</th>
	          <th>状态</th>
			<th>操作</th>
	        </tr>
	      </thead>
	      <tbody>

	      <?php foreach ($bus as $item): ?>
	      	 
	      	 <tr>
	          <th scope="row"><?php echo $item->bid; ?></th>
	          <td><?php echo $item->bname; ?></td>
	          <td><?php echo $item->bnumber; ?></td>
	          <td><?php echo $item->bbrand; ?></td>
	          <td><?php echo $item->a; ?></td>
	          <td><?php echo $item->b; ?></td>
	          <td><?php echo $item->c; ?></td>
	          <td><?php echo $item->d; ?></td>
	          <td><?php echo $item->e; ?></td>
	          <td><?php echo $item->f; ?></td>
	          <td><?php echo $item->g; ?></td>
	          <td><?php echo $item->h; ?></td>
	          <td><?php echo $item->state; ?></td>
	          <td><button class="del" onclick="del(<?php echo $item->bid; ?>)">删除</button></td>
	        </tr>
	      <?php endforeach ?>
	        
	        
	      </tbody>
	    </table>

	</div>
</div>
 
</body>
<script type="text/javascript" src="js/bootstrap/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="js/bootstrap/bootstrap-datetimepicker.zh-CN.js"></script>
<script>
	function del(id){
		if(confirm('确定删除？'))
		$.ajax({
			url: SITE_URL+'/bus/delete',
			type: 'get',		
			data: {
				id: id
			},
			success: function(d){
				if(d.state){
					alert('删除成功');
					window.location.reload();
				}				
			},
			error: function(){
		
			},
			dataType: 'json'
		})
	}
</script>
</html>