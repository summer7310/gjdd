<!DOCTYPE html>
<head>
<title>集市宝</title>
<?php $this->load->view('common/source');?>
<script type="text/javascript">
	var $_GET = function(){
	    var url = window.document.location.href.toString();
	    var u = url.split("?");
	    if(typeof(u[1]) == "string"){
	        u = u[1].split("&");
	        var get = {};
	        for(var i in u){
	            var j = u[i].split("=");
	            get[j[0]] = j[1];
	        }
	        return get;
	    } else {
	        return {};
	    }
	} 
	$(document).ready(function(){
		$('.cg-con').click(function(){
			var id = $(this).attr('rel');
			$('.cg-con').removeClass('current');
			$(this).addClass('current');
			$('.con').css('display', 'none');
			$('#' + id).css('display', 'block');
		});

		 

		if($_GET().type == 'setmycoupon'){
			$('#setmycoupon').addClass('current');
			$('.con').css('display', 'none');
			$('#con-1').css('display', 'block');
		}
		else if($_GET().type == 'exchangecoupon'){
			$('#exchangecoupon').addClass('current');
			$('.con').css('display', 'none');
			$('#con-2').css('display', 'block');
		}
		else {
			$('#setmycoupon').addClass('current');
		}
	});
</script>
</head>
<body class="dark">
	<header class="layout-header">
		<div class="header">
			我的优惠券<a class="button-left"
				href="<?php echo site_url('user/index')?>"><i
				class="fa fa-chevron-left"></i></a>
		</div>
	</header>
	<div class="layout">
		<ul id="menu" class="tabs tabs-2">
			<li><a class="cg-con" id="setmycoupon" rel="con-1" href="javascript:void(0)">优惠码兑换</a></li>
			<li><a class="cg-con" id="exchangecoupon" rel="con-2" href="javascript:void(0)">余额兑换</a></li>	 
		</ul>
	</div>
		<div class="list-item"  >

			<div class="row con" id="con-1">
				<div class="col-8 col-pd">
					<input class="input input-block" type="text" name="couponno" id="couponno" placeholder="请输入优惠码">
				</div>
				<div class="col-4 col-pd">
					<input type="button" class="button button-blank button-block" id="submit_btn" value="提 交">
				</div>
			</div>
			<div class="con" id="con-2" style="display:none;">
				<div class="row">
					 
					<div class="col-8 col-pd">
						<input class="input input-block" type="text" name="couponno" id="num" placeholder="请输入数量">
						<input type="hidden" name="total" value="<?php echo $user['balance'];?>">
					</div>
					<div class="col-4 col-pd">
						<input type="button" class="button button-blank button-block" id="exchange_btn" value="兑 换">
					</div>

					
				</div>
				<div class="row">
					<i class="fa fa-exclamation-circle"></i> 余额：<?php echo $user['balance'];?>元
				优惠券：满100元减20元
				</div>
			</div>
		</div>
		<?php foreach ($coupons as $item):?>
			<div class="list-item coupon radio-box" name="coupon" value="<?php echo $item['id'];?>">
				<h3>
					满 <span><?php echo $item['more'];?></span>元减
					<span><?php echo $item['off'];?></span>
				元</h3>
				
				<ul class="row">
					<li class="col-12 order-info">
						<span class="cat">优惠券编号</span>
						<?php echo $item['couponno'];?>
					</li>
					<li class="col-12  order-info">
						<span class="cat">有效期</span>
						<?php echo date("Y-m-d",strtotime($item['starttime']));?> 至 <?php echo date("Y-m-d",strtotime($item['endtime']));?>
					</li>
					<li class="col-12 order-info">
						<span class="cat">状 态</span>
						<?php
							if ($item ['state'] == 0) {
								echo "可用";
							}
							?>
					</li>
				</ul>
			</div>
		<?php endforeach;?>
</body>
<script>
	$(document).ready(function() {
		//$("#submit_btn").bind("click",function(event){
		$("#submit_btn").click(function(){
			var couponno = $("#couponno").val();
			if(couponno==""){
				alert("优惠码不能为空!");
				return false;
			}
			$.ajax({
				type : "post",
				url : "<?php echo site_url('coupon/setmycoupon')?>",
				//async : false,
				data : {
					"couponno" : couponno
				},
				dataType : "json",
				success : function(data) {
					if(data.code){
						alert(data.msg);
						window.location.href='<?php echo site_url('coupon/mycoupon')?>?type=setmycoupon';
					} else {
						alert(data.msg);
					}
				}
			});
		});

		$("#exchange_btn").click(function(){
			var num = $("#num").val();
			var total = $("input[name='total']").val();

			if(num=="" || num<=0){
				alert("兑换优惠券数量不能为空或零!");
				return false;
			}
			if(/\d/.test(num)){
				if(num >  parseInt(total/20)){
					alert("余额不足");
					return false;
				}

			} else {
				alert("兑换优惠券数量必须为数字");
				return false;
			}
			$.ajax({
				type : "post",
				url : "<?php echo site_url('coupon/exchangecoupon')?>",
				//async : false,
				data : {
					"num" : num
				},
				dataType : "json",
				success : function(data) {
					if(data.code){
						alert(data.msg);
						window.location.href='<?php echo site_url('coupon/mycoupon')?>?type=exchangecoupon';
					} else {
						alert(data.msg);
					}
				}
			});
		});
	}); 
</script>