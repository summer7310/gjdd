<!DOCTYPE html>
<head>
<title>集市宝</title>
<?php $this->load->view('common/source');?>
<style type="text/css">
	.list-title{
		padding:0.5em 1em 0.4em 1em;
	}
	body{
		background: #f9f9f9;
	}
	.ad-box{
		background: #fff;
		margin:10px 0;
		border:1px #eee solid;
		padding: 10px;
	}
	.ad-box-item{
		overflow: hidden;

	}
	.ad-box-title, .ad-box-content{
		line-height: 28px;

	}

	.ad-box-bor{
		border-bottom:1px #eee solid;
		padding-bottom: 10px;
		margin-bottom: 10px;
	}

	.ad-box-title{
		float:left;
		width: 70px;
		text-align: left;
		font-weight: bold;
		 
		color:#34ad8f;
	}

	.ad-box-content{
		margin-left: 70px;
		text-align: left;
	}
</style>
</head>
<body>
	<header class="layout-header">
		<div class="header">
			新品预定<a class="button-left"
				href="<?php echo site_url('user/index')?>"><i
				class="fa fa-chevron-left"></i></a>
		</div>
	</header>
	<section class="layout">
		<form autocomplete="off" id="form">
		<h3 class="list-title">产品名称</h3>
		<div class="list-item list-item-np">
			<input class="input input-block input-simple verify-required" type="text" name="name" id="name" placeholder="请输入产品名称">
		</div>
		<h3 class="list-title">描述</h3>
		<div class="list-item list-item-np">
			<input class="input input-block input-simple verify-required" type="text" name="description" id="description" placeholder="请输入描述">
		</div>
		<h3 class="list-title">预约数量</h3>
		<div class="list-item list-item-np">
			<input class="input input-block input-simple verify-required" type="text" name="amount" id="amount" placeholder="请输入预约数量">
		</div>
		<div class="row row-pd">
			<input type="button" class="button button-default button-block" id="submit_btn" value="提交">
		</div>
		</form>
		 
			<?php 	foreach ( $newproducts as $item ) :?>
			<div class="ad-box">
				<div class="ad-box-item ad-box-bor">
					<div class="ad-box-title">
						新品名称:
					</div>
					<div class="ad-box-content">
						<?php echo $item['name']; ?>
					</div>
				</div>
				<div class="ad-box-item"> 
					<div class="ad-box-title">回 复:</div>
					<div class="ad-box-content">
					<?php 
						if(empty($item['reply']))
							echo '暂无回复'; 
						else 
							echo $item['reply']; 
					?>
					</div>
				</div>
			</div>
			<?php 	endforeach;?>
		
	</section>
</body>
<script>
	$(document).ready(function() {
		$("#submit_btn").click(function(){
			if(!leeui.verify.check('#form')){
				return ;
			}
			var name = $("#name").val();
			var description = $("#description").val();			 
			var amount = $("#amount").val();
			 
			$.ajax({
				url : '<?php echo site_url('user/saveproduct')?>',
				type : "post",
				//async : false,
				data : {
					'name' : name,
					'description' : description,
					'amount' : amount
				},
				dataType : "json",
				success : function(data) {
					if(data.code)
					{
						alert(data.msg);
						window.location.reload();
					}else{
						alert(data.msg);
					}
				}
			});
		});
	}); 
</script>