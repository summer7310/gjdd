<!DOCTYPE html>
<head>
<title>集市宝</title>
<?php $this->load->view('common/source');?>
<style type="text/css">
	.list-title{
		padding:0.5em 1em 0.4em 1em;
	}
	body{
		background: #f9f9f9;
	}
</style>
</head>
<body>
	<header class="layout-header">
		<div class="header">
			修改收货时间<a class="button-left"
				href="<?php echo site_url('user/index')?>"><i
				class="fa fa-chevron-left"></i></a>
		</div>
	</header>
	<section class="layout">
		<form autocomplete="off">
		
		<h3 class="list-title">最早收货时间</h3>
		<div class="list-item list-item-np list-input">
			<i class="fa  fa-chevron-down icon-right"></i>
			<select id="early" data-time="7:00-10:30" data-jump="30" data-select="<?php echo $early_time;?>" style="-webkit-appearance: none;background:#fff" class="input input-block input-simple timepicker">					
             
            </select>
		</div>
		<h3 class="list-title">最晚收货时间</h3>
		<div class="list-item list-item-np list-input">
			<i class="fa  fa-chevron-down icon-right"></i>
			<select id="late" data-time="7:00-10:30" data-jump="30" data-select="<?php echo $late_time;?>" class="input input-block input-simple timepicker">					
            	  
            </select>
		</div>
		<div class="row row-pd">
			<input type="hidden" id="redirect" value="<?=$redirect?>" />
			<input type="hidden" id="orderid" value="<?=$orderid?>" />
			<input type="button" class="button button-default button-block" id="submit_btn" value="修改">
		</div>
		</form>
	</section>
</body>
<script>
	$(document).ready(function() {
		$("#submit_btn").click(function(){
			var early = $("#early").val();
			var late = $("#late").val();
			var redirect = $("#redirect").val();
			var orderid = $("#orderid").val();
			$.ajax({
				url : '<?php echo site_url('user/savedate')?>',
				type : "post",
				data : {
					'early' : early,
					'late' : late
				},
				dataType : "json",
				success : function(data) {
					if(data.code)
					{
						alert(data.msg);
						if(redirect==1){
							window.location.href='<?php echo site_url('order/pay/id')?>'+'/'+orderid;
						}
					}else{
						alert(data.msg);
					}
				}
			});
		});
	}); 
</script>
<script type="text/javascript" src="js/timepicker.js"></script>