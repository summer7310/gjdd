<!DOCTYPE html>
<head>
<title>集市宝</title>
<?php $this->load->view('common/source');?>
<script type="text/javascript" src="js/sea.js"></script>
<script type="text/javascript">
var SITE_URL = "<?php echo site_url('')?>";
var BASE_URL = "<?php echo base_url('')?>";
 
seajs.config({
	base: BASE_URL + '/js',
	paths: {
    	'static': BASE_URL + '/static',
    	'modules': BASE_URL + '/js/modules1.3.0' 
  	},
  alias: {
    "zepto": "zepto.min.js?v=1.0.1",
    "zepto.mtimer": "zepto.mtimer.js",
   // "fastclick": "fastclick.min.js",
    "iscroll": "iscroll.min.js",
    "leeui": "static/leeui.min.js?v=1.3.6"
  }
})
seajs.use("main.balance.js?v=1.0");
</script>
</head>
<body>
	<header class="layout-header">
		<div class="header">
			余额收支明细
			<a class="button-left" href="<?php echo site_url('user/index')?>"><i class="fa fa-chevron-left"></i></a>
		</div>
	</header>

		<section class="layout" id="balance-list">
			<div class="none" id="empty" style="display:none;">
				<p class="icon">
					<i class="fa  fa-meh-o"></i>
				</p>
				<p>没有相关信息</p>
				 
			</div>	 
	</section>
	
</body>