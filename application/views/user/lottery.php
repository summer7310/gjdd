<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1,user-scalable=no">
<title>积分抽奖</title>
<base href="<?php echo site_url('')?>"/>
<link rel="stylesheet" type="text/css" href="static/leeui.min.css?v=1.9.9">
<link href="static/lottery.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="static/fonts/font.min.css">
<style type="text/css">
	.score-notice{
		color:#FFBE04;
		font-size: 15px;
	}
	.score{
		background: #FFBE04;
		color: #E62D2D;
		font-size: 18px;
		padding:10px 20px;
		display: inline-block;
		margin:20px 0;
		border-radius: 18px;
	}
</style>
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/awardRotate.js"></script>
<script type="text/javascript">
var turnplate={
		restaraunts:[],				//大转盘奖品名称
		colors:[],					//大转盘奖品区块对应背景颜色
		outsideRadius:192,			//大转盘外圆的半径
		textRadius:155,				//大转盘奖品位置距离圆心的距离
		insideRadius:68,			//大转盘内圆的半径
		startAngle:0,				//开始角度
		
		bRotate:false				//false:停止;ture:旋转
};

$(document).ready(function(){
	//动态添加大转盘的奖品与奖品区域背景颜色
	turnplate.restaraunts = ["5元优惠券", "500积分", "谢谢参与", "1000积分", "10元优惠券", "20元优惠券", "5000积分 ", "30元优惠券", "50元优惠券", "100元优惠券"];
	turnplate.colors = ["#FFF4D6", "#FFFFFF", "#FFF4D6", "#FFFFFF","#FFF4D6", "#FFFFFF", "#FFF4D6", "#FFFFFF","#FFF4D6", "#FFFFFF"];
	turnplate.texts = ["恭喜您获得了满250减5元优惠券", "恭喜您获得了500积分", "谢谢参与", "恭喜您获得了1000积分", "恭喜您获得了满350减10元优惠券", "恭喜您获得了满500减20元优惠券", "恭喜您获得了5000积分 ", "恭喜您获得了满800减30元优惠券", "恭喜您获得了满1000减50元优惠券", "恭喜您获得了满1500减100元优惠券"];
	
	var rotateTimeOut = function (){
		$('#wheelcanvas').rotate({
			angle:0,
			animateTo:2160,
			duration:8000,
			callback:function (){
				alert('网络超时，请检查您的网络设置！');
			}
		});
	};

	 
	//旋转转盘 item:奖品位置; txt：提示语;
	var rotateFn = function (item, txt, res){
		var angles = item * (360 / turnplate.restaraunts.length) - (360 / (turnplate.restaraunts.length*2));
		if(angles<270){
			angles = 270 - angles; 
		}else{
			angles = 360 - angles + 270;
		}
		$('#wheelcanvas').stopRotate();
		$('#wheelcanvas').rotate({
			angle:0,
			animateTo:angles+1800,
			duration:8000,
			callback:function (){
				alert(txt);
				$('#score-num').text(res);
				turnplate.bRotate = !turnplate.bRotate;
			}
		});
	};

	$('.pointer').click(function (){
		if(parseInt($('#score-num').text()) >= 1000){
			if(turnplate.bRotate)return;
			turnplate.bRotate = !turnplate.bRotate;
			//获取随机数(奖品个数范围内)
			var item = rnd(1,turnplate.restaraunts.length);

			$.ajax({
					url : '<?php echo site_url('user/lotteryresult')?>',
					type : "post",
					//async : false,
					data : {
						'type' : item
					},
					dataType : "json",
					success : function(data) {
						if(data.code)
						{
							var res = data.data;
							//奖品数量等于10,指针落在对应奖品区域的中心角度[252, 216, 180, 144, 108, 72, 36, 360, 324, 288]
							rotateFn(item, turnplate.texts[item-1], res);
							 
						}else{
							alert(data.data);
						}
					}
			});
			//console.log(item);
		} else {
			alert('积分不足');
		}
	

	});
});

function rnd(n, m){
	var random = Math.floor(Math.random()*10*(m-n+1)+n);
	if(random<=28){
		random=1;
	}else if(random<=33){
		random=5;
	}else if(random<=38){
		random=6;
	}else if(random<=40){
		random=8;
	}else if(random<=41){
		random=9;
	}else if(random<=42){
		random=10;
	}else if(random<=92){
		random=3;
	}else if(random<=97){
		random=2;
	}else if(random<=99){
		random=4;
	}else{
		random=7;
	}
	
	return random;
	
}


//页面所有元素加载完毕后执行drawRouletteWheel()方法对转盘进行渲染
window.onload=function(){
	drawRouletteWheel();
};

function drawRouletteWheel() {    
  var canvas = document.getElementById("wheelcanvas");    
  if (canvas.getContext) {
	  //根据奖品个数计算圆周角度
	  var arc = Math.PI / (turnplate.restaraunts.length/2);
	  var ctx = canvas.getContext("2d");
	  //在给定矩形内清空一个矩形
	  ctx.clearRect(0,0,422,422);
	  //strokeStyle 属性设置或返回用于笔触的颜色、渐变或模式  
	  ctx.strokeStyle = "#FFBE04";
	  //font 属性设置或返回画布上文本内容的当前字体属性
	  ctx.font = '16px Microsoft YaHei';      
	  for(var i = 0; i < turnplate.restaraunts.length; i++) {       
		  var angle = turnplate.startAngle + i * arc;
		  ctx.fillStyle = turnplate.colors[i];
		  ctx.beginPath();
		  //arc(x,y,r,起始角,结束角,绘制方向) 方法创建弧/曲线（用于创建圆或部分圆）    
		  ctx.arc(211, 211, turnplate.outsideRadius, angle, angle + arc, false);    
		  ctx.arc(211, 211, turnplate.insideRadius, angle + arc, angle, true);
		  ctx.stroke();  
		  ctx.fill();
		  //锁画布(为了保存之前的画布状态)
		  ctx.save();   
		  
		  //----绘制奖品开始----
		  ctx.fillStyle = "#E5302F";
		  var text = turnplate.restaraunts[i];
		  var line_height = 22;
		  //translate方法重新映射画布上的 (0,0) 位置
		  ctx.translate(211 + Math.cos(angle + arc / 2) * turnplate.textRadius, 211 + Math.sin(angle + arc / 2) * turnplate.textRadius);
		  
		  //rotate方法旋转当前的绘图
		  ctx.rotate(angle + arc / 2 + Math.PI / 2);
		  
		  /** 下面代码根据奖品类型、奖品名称长度渲染不同效果，如字体、颜色、图片效果。(具体根据实际情况改变) **/
		  if(text.indexOf("元")>0){//流量包
			  var texts = text.split("元");
			  for(var j = 0; j<texts.length; j++){
				  ctx.font = j == 0?'26px Microsoft YaHei':'16px Microsoft YaHei';
				  if(j == 0){
					  ctx.fillText(texts[j]+"元", -ctx.measureText(texts[j]+"元").width / 2, j * line_height);
				  }else{
					  ctx.fillText(texts[j], -ctx.measureText(texts[j]).width / 2, j * line_height);
				  }
			  }
		  } else if(text.indexOf("积")>0){
		  	 var texts = text.split("积");
		  	texts[1] = '积分';
			  for(var j = 0; j<texts.length; j++){
				  ctx.font = j == 0?'26px Microsoft YaHei':'16px Microsoft YaHei';
				  if(j == 0){
					  ctx.fillText(texts[j], -ctx.measureText(texts[j]).width / 2, j * line_height);
				  }else{
					  ctx.fillText('积分', -ctx.measureText(texts[j]).width / 2, j * line_height);
				  }
			  }
		  }else if(text.indexOf("元") == -1 && text.length>6){//奖品名称长度超过一定范围 
			  text = text.substring(0,6)+"||"+text.substring(6);
			  var texts = text.split("||");
			  for(var j = 0; j<texts.length; j++){
				  ctx.fillText(texts[j], -ctx.measureText(texts[j]).width / 2, j * line_height);
			  }
		  }else{
			  //在画布上绘制填色的文本。文本的默认颜色是黑色
			  //measureText()方法返回包含一个对象，该对象包含以像素计的指定字体宽度
			  ctx.fillText(text, -ctx.measureText(text).width / 2, 0);
		  }
		  
		  //把当前画布返回（调整）到上一个save()状态之前 
		  ctx.restore();
		  //----绘制奖品结束----
	  }     
  } 
}

</script>
</head>
<body style="background:#e62d2d;overflow-x:hidden;">
	<header class="layout-header">
		<div class="header" style="background:#e62d2d;overflow-x:hidden;">
			积分抽奖
			<a class="button-left" href="<?php echo site_url('user/score')?>"><i class="fa fa-chevron-left" style="line-height:44px !important"></i></a>
		</div>
	</header>
<br>
<!-- 代码 开始 -->
<div>
	<p class="score-notice"><i class="fa fa-exclamation-circle"></i> 积分抽奖区  1000积分/次</p>
	<p class="score"><span id="score-num"><?php echo $score; ?></span> 积分</p>
</div>
<div class="banner">
	<div class="turnplate" style="background-image:url(img/turnplate-bg.png);background-size:100% 100%;">
		<canvas class="item" id="wheelcanvas" style="z-index:99;" width="422px" height="422px">浏览器不支持canvas</canvas>
		<img class="pointer" style="z-index:99999;" src="img/turnplate-pointer.png"/>
	</div>
</div>
<!-- 代码 结束 -->

</body>
</html>