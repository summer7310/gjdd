<!DOCTYPE html>
<html>
<head>
<title>个人中心</title>
<?php $this->load->view('common/include');?>
</head>
<body id="app">
	<header-layout title='Bone Vue'>
		<header-dock align='left'>
			<a href="<?php echo site_url('/home'); ?>"><Icon type="back"></Icon></a>
		</header-dock>
		个人中心		 
	</header-layout>
	<layout-main>
		<layout-box>
			<tabs-vertical>
				<tabs-item-vertical arrow=true iconcss=true icon='profile'>
					<a href="<?php echo site_url('/user/edit') ; ?>">个人资料</a>
				</tabs-item-vertical>
				<tabs-item-vertical arrow=true iconcss=true icon='sort'>
					<a href="<?php echo site_url('/order/myorder') ; ?>">我的订单</a>
				</tabs-item-vertical>
			</tabs-vertical>
		</layout-box>
		<grid-row p='1111'>
			<Btn type="warm" size="lg" block=true  name="退出登录" @btn-click="logout"></Btn>
		</grid-row>			
	</layout-main>
	<script src="js/user.build.js"></script>
</body>
</html>
