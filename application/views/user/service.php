<!DOCTYPE html>
<head>
<title>集市宝</title>
<?php $this->load->view('common/source');?>
<style type="text/css">

	.list-title{
		padding:0.5em 1em 0.4em 1em;
	}
	body{
		background: #f9f9f9;
	}
	.contact{
		font-size: 18px;
		text-align: left;

	}
	.contact .call{
		position: absolute;
		right:0;
		top:0;
		padding: 18px 15px;
	}
	.contact .call a{
		color: #dd514c;
	}
</style>
</head>
<body>
	<header class="layout-header">
		<div class="header">
			联系客服<a class="button-left"
				href="<?php echo site_url('user/index')?>"><i
				class="fa fa-chevron-left"></i></a>
		</div>
	</header>
	<?php  
	     if(!empty($promoter['id'])){ 
		      if($promoter['id']<20000){
	?>
	<h3 class="list-title">销售经理</h3>
	<div class="list-item contact">
		<a href="tel:<?php echo $promoter['phone'];?>">
			<i class="fa fa-mobile"></i> <?php echo $promoter['phone'];?>
		</a>
		<div class="call">
			<a href="tel:<?php echo $promoter['phone'];?>">拨 打</a>
		</div>
	</div>
	<?php }else{
			if(!empty($user['service'])){	
	?>
	<h3 class="list-title">销售经理</h3>
	<div class="list-item contact">
		<a href="tel:<?php echo $user['service'];?>">
			<i class="fa fa-mobile"></i> <?php echo $user['service'];?>
		</a>
		<div class="call">
			<a href="tel:<?php echo $user['service'];?>">拨 打</a>
		</div>
	</div>	
	<?php }}}?>
	<h3 class="list-title">客服电话 <span>9:50-17:00 周一至周五</span></h3>
	<div class="list-item contact">
		<a href="tel:0571-87006239">
			<i class="fa  fa-phone"></i> 0571-87006239
		</a>
		<div class="call">
			<a href="tel:0571-87006239">拨 打</a>
		</div>
	</div>
</body>