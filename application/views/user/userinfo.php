<!DOCTYPE html>
<head>
<title>集市宝</title>
<?php $this->load->view('common/source');?>
</head>
<body class="dark">
	<header class="layout-header">
		<div class="header">
			<?php echo $username;?>的资料
			<a class="button-left" href="<?php echo site_url('user/index')?>"><i class="fa fa-chevron-left"></i></a>
			<a class="button-right" href="<?php echo site_url('user/edit')?>"><i class="fa"></i>编辑资料</a>
		<!-- 	<a class="button-right" href=""><i class="fa fa-user"></i></a> -->
		</div>
	</header>
	<div class="layout">
		<h3 class="list-title">用户名</h3>
		<ul class="list">
			<li class="list-item">
				<?php echo $username;?>
			</li>
		</ul>
		<h3 class="list-title">店 名</h3>
		<ul class="list">
			<li class="list-item">
				<?php  echo $shopname;?>
			</li>
		</ul>	
		<h3 class="list-title">地 址</h3>
		<ul class="list">
			<li class="list-item">
				<?php echo $address;?>
			</li>
		</ul> 
		<h3 class="list-title">联系人</h3>
		<ul class="list">
			<li class="list-item">
				<?php echo $contact;?>
			</li>
		</ul>
		<h3 class="list-title">手机号</h3>
		<ul class="list">
			<li class="list-item">
				<?php echo $cellphone;?>
			</li>
		</ul>
	</div>
 
</body>
</html>