<!DOCTYPE html>
<html>
<head>
<title>添加分类</title>
<?php $this->load->view('common/include');?>
<style>

</style>
</head>
<body id="app" v-cloak>
		<header-layout title='Bone Vue'>
			<header-dock align='left'>
				<a href="<?php echo site_url().'/home' ?>"><Icon type="back"></Icon></a>
			</header-dock>
			用户管理		
			<header-dock align='right'>
				<a href="<?php echo site_url().'/user/register' ?>">添加用户</a>
			</header-dock>	
		</header-layout>
		<layout-main>			
				<layout-box p="1111" v-for="item in users">
					<div class="bo-row bo-ovf " data-id="{{item.id}}">
						<div class="bo-col-3">
						 	<div class="info-item">
						 		<h3 class="name">用户名</h3>
						 		<p class="content">
						 			{{item.name}}
						 		</p>
						 	</div>
						 </div>	
						 <div class="bo-col-5">
						 	<div class="info-item">
						 		<h3 class="name">Email</h3>
						 		<p class="content">
						 			{{item.email}}					 		
						 		</p>
						 	</div>
						 </div>	

						 <div class="bo-col-4">
						 	<div class="info-item">
						 		<h3 class="name">类别</h3>
						 		<p class="content">
						 			{{roles[item.role-1].text}}					 		
						 		</p>
						 	</div>
						 </div>	
						 					  
					 </div>
					 <div class="bo-row" style="text-align:right">
					 
					 	<a class="bo-btn-blank" href="<?php echo site_url('user/edit'); ?>?uid={{item.id}}">编 辑</a>
					 	<btn @btn-click="delete(item.id)" blank=true name="删 除"></btn>
					 </div>
				</layout-box>	
				 			  	

		</layout-main>
	<script src="js/user.build.js"></script>
</body>
</html>
