<!DOCTYPE html>
<head>
<title>集市宝</title>
<?php $this->load->view('common/source');?>
</head>
<body>
	<header class="layout-header">
		<div class="header">
			<?php echo $username;?>的账户
			<a class="button-left" href="<?php echo site_url('user/index')?>"><i class="fa fa-chevron-left"></i></a>
		</div>
	</header>
	<div class="layout">
		<h3 class="list-title">用户名</h3>
		<ul class="list">
			<li class="list-item">
				<?php echo $username;?>
			</li>
		</ul>
		<h3 class="list-title">余额</h3>
		<ul class="list">
			<li class="list-item">
				<?php echo $balance;?>元
			</li>
		</ul>
		<h3 class="list-title">积分</h3>
		<ul class="list">
			<li class="list-item">
				<?php echo $score;?>分
			</li>
		</ul>
	</div>
 
</body>
</html>