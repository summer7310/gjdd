<!DOCTYPE html>
<html>
<head>
<title>注册新用户</title>
<?php $this->load->view('common/include');?>
<style>

</style>
</head>
<body id="app" v-cloak>
		<header-layout title='Bone Vue'>
			<header-dock align='left'>
				<a href=""><Icon type="back"></Icon></a>
			</header-dock>
			编辑用户			
		</header-layout>
		<layout-main>
			
			<form-group type='blank' v-ref:form>
				<layout-box >
					<form-input v-ref:phone val="<?php echo $user->email; ?>" name="email" format="email" required=true place-holder="请输入账号" label="账 号" icon="people" size="lg"></form-input>					 
					<form-input val="val="<?php echo $user->password; ?>"" name="password" required=true place-holder="请输入密码" label="密 码" type="password" icon="lock" size="lg"></form-input>								 
					<form-input val="<?php echo $user->name; ?>" name="name" required=true place-holder="请输入姓名" label="姓 名" size="lg"></form-input>
					<form-select  

						:data="roles" 
						name="role" 
						required=true 
						place-holder="请选择角色" 
						label="身 份" 
						size="lg"
						 
						></form-select>
				</layout-box>	
				 
			</form-group>
						  	
			<grid-row p='1111'>
				<Btn type="primary" size="lg" block=true @btn-click="submit" name="提 交"></Btn>
			</grid-row>
		</layout-main>
	<script src="js/register.build.js"></script>
</body>
</html>
