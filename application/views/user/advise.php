<!DOCTYPE html>
<head>
<title>集市宝</title>
<?php $this->load->view('common/source');?>
<style type="text/css">
	.list-title{
		padding:0.5em 1em 0.4em 1em;
	}
	body{
		background: #f9f9f9;
	}
			.ad-box{
			background: #fff;
			margin:10px 0;
			border:1px #eee solid;
			padding: 10px;
		}
		.ad-box-item{
			overflow: hidden;

		}
		.ad-box-title, .ad-box-content{
			line-height: 28px;

		}

		.ad-box-bor{
			border-bottom:1px #eee solid;
			padding-bottom: 10px;
			margin-bottom: 10px;
		}

		.ad-box-title{
			float:left;
			width: 50px;
			text-align: left;
			font-weight: bold;
			 
			color:#34ad8f;
		}

		.ad-box-content{
			margin-left: 50px;
		text-align: left;
		}
</style>
</head>
<body>
	<header class="layout-header">
		<div class="header">
			投诉建议<a class="button-left"
				href="<?php echo site_url('user/index')?>"><i
				class="fa fa-chevron-left"></i></a>
		</div>
	</header>
	<section class="layout">
		<form autocomplete="off" id="form">
		<h3 class="list-title">内容</h3>
		<div class="list-item list-item-np">
			<textarea rows="8" class="input input-block input-simple verify-required" name="content" id="content" placeholder="请输入内容"></textarea>
		</div>
		<div class="row row-pd">
			<input type="button" class="button button-default button-block" id="submit_btn" value="提交">
		</div>
		</form>
	 
		
			<?php 	foreach ( $advises as $item ) :?>
			<div class="ad-box">
				<div class="ad-box-item ad-box-bor">
					<div class="ad-box-title">建议：</div>
					<div class="ad-box-content">
						<?php echo $item['content']; ?>
					</div>
					
				</div>
				<div class="ad-box-item"> 
					<div class="ad-box-title">回复：</div>
					<div class="ad-box-content">
					<?php 
						if(empty($item['reply']))
							echo '暂无回复'; 
						else 
							echo $item['reply']; 
					?>
					</div>
				</div>
			</div>
			<?php 	endforeach;?>
		
	</section>
</body>
<script>
	$(document).ready(function() {
		$("#submit_btn").click(function(){
			var content = $("#content").val().trim();
			 if(!leeui.verify.check('#form')){
				return ;
			}
			$.ajax({
				url : '<?php echo site_url('user/saveadvise')?>',
				type : "post",
				//async : false,
				data : {
					'content' : content
				},
				dataType : "json",
				success : function(data) {
					if(data.code)
					{
						alert(data.msg);
						window.location.reload();
					}else{
						alert(data.msg);
					}
				}
			});
		});
	}); 
</script>