<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<title>城市物流配送系统 </title>
	<?php include('source.php'); ?>

</head>
<body>
<!-- 导航栏 -->	

 <?php include('menu.php'); ?>
<div class="container" style="width: 100%;padding-top:20px;">
	
	<?php include('side.php'); ?>
	<div class="col-sm-10">
		<table class="table">
      <caption>司机列表</caption>
      <thead>
        <tr>
          <th>#</th>
          <th>姓名</th>
          <th>身份证号</th>
          <th>驾龄</th>
          <th>性别</th>
          <th>车辆</th>
        </tr>
      </thead>
      <tbody>
       	<?php foreach ($drivers as $item): ?>
	      	 
	      	 <tr>
	          <th scope="row"><?php echo $item->did; ?></th>
	          <td><?php echo $item->dname; ?></td>
	          <td><?php echo $item->dcredit; ?></td>
	          <td><?php echo $item->driveAge; ?></td>
	          <td><?php echo $item->dmale; ?></td>
	           <td><?php echo $item->bname; ?></td>
	        
	          <td><button class="del" onclick="del(<?php echo $item->did; ?>)">删除</button></td>
	        </tr>
	      <?php endforeach ?>
      </tbody>
    </table>
	</div>
</div>
 
</body>
<script type="text/javascript" src="js/bootstrap/bootstrap-datetimepicker.min.js"></script>
<script type="text/javascript" src="js/bootstrap/bootstrap-datetimepicker.zh-CN.js"></script>
<script>
	function del(id){
		if(confirm('确定删除？'))
		$.ajax({
			url: SITE_URL+'/drivers/delete',
			type: 'get',		
			data: {
				id: id
			},
			success: function(d){
				if(d.state){
					alert('删除成功');
					window.location.reload();
				}				
			},
			error: function(){
		
			},
			dataType: 'json'
		})
	}
</script>
</html>