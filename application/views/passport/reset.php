<!DOCTYPE html>
<head>
<title>集市宝</title>
<?php $this->load->view('common/source');?>
<style type="text/css">
	.list-title{
		padding:0.5em 1em 0.4em 1em;
	}
	body{
		background: #f9f9f9;
	}
	.phonecheck{
		position: relative;
	}

	.phonecheck #send{
		position: absolute;
		right:0;
		top:0;
	}

	.button-phone{
		height: 42px;
		background: #fff;
		border:0;
		border-left:1px #ccc solid;
		padding:0 10px;
		background: #3ac19f;
		color: #fff;
	}

	.button-phone:hover{
		background: #3ac19f;
		color: #fff;
	}

	.left-button{
		color: #444;
		background: #fff;
	}

	.left-button:hover{
		background: #fff;
		color: #444;
	}


</style>
</head>
<body>
	<header class="layout-header">
		<div class="header">
			修改密码 <a class="button-left"
				href="<?php echo site_url('passport/index')?>"><i class="fa fa-chevron-left"></i></a>
		</div>
	</header>
	<section class="layout">
		<form id="form" class="verify"  onsubmit="return false;" method="post" autocomplete="off">		 
		<h3 class="list-title">手机号</h3>
		<div class="list-item list-item-np phonecheck">
			<input class="input input-block input-simple verify-required" type="text" name="phone"  id="phone" placeholder="请输入手机号">
			<button class="button-phone fr" id="send">发送验证码</button>
		</div>
		<h3 class="list-title">验证码</h3>
		<div class="list-item list-item-np">
			<input class="input input-block input-simple verify-required" type="text" name="code" id="code" placeholder="请输入验证码">
		</div>
		<h3 class="list-title">新密码</h3>
		<div class="list-item list-item-np">
			<input class="input input-block input-simple verify-required" type="password" name="password" id="password" placeholder="请输入新密码">
		</div>
		<h3 class="list-title">确认新密码</h3>
		<div class="list-item list-item-np">
			<input class="input input-block input-simple verify-required" type="password" name="password2" id="password2" placeholder="重新输入新密码">
		</div>
		 
		<div class="row row-pd">
			<input type="button" class="button button-default button-block" id="submit_btn" value="确认修改">
		</div>
		</form>
		 
	</section>
</body>
<script>
$(document).ready(function(){
	var phonecheck = false;
	//var timer;
	//发送验证码
	$('#send').click(function(){
		if(phonecheck) return false;
		//
		var verify = /[0-9]{11}/;
		if(!verify.test($('#phone').val())){
			leeui.verify.set({
				'#phone': '号码格式不正确'
			})
			return false;
		} else {
			leeui.verify.set({
				'#phone': ''
			})
		}

		phonecheck = true;
		var clock = 60;//设置计时
		var text = $(this).text();		
		var $this = $(this);
		$this.addClass('left-button');
		var timer = setInterval(function(){
			--clock;
			$this.text('剩余 ' + clock + ' 秒');
			if(clock==0){
				$this.removeClass('left-button');
				//clock = 60;
				$this.text(text);
				clearInterval(timer);
				phonecheck = false;
			}
		},1000);
		
		var phone = $("#phone").val();
			$.ajax({
				url : '<?php echo site_url('passport/sendCode')?>',
				type : "post",
				data : {
					'phone' : phone,
				},
				dataType : "json",
				success : function(data) {
					if(data.code)
					{
						leeui.dialog('', data.msg).ok(function(e){
							e.close();
						});
					}else{
						leeui.dialog('', data.msg).ok(function(e){
							e.close();
						});;
					}
				}
			});
			
		return false;
	});
	//密码
	$('#submit_btn').click(function(){
		//clearInterval(timer);
		if(!leeui.verify.check('.verify')){
			return false;
		}
		if($('input[name="password"]').val() != $('input[name="password2"]').val()){
			leeui.verify.set({
				'input[name="password2"]': '两次密码输入不一致'
			});
			return false;
		}
		var phone = $("#phone").val();
		var password = $("#password").val();
		var code = $("#code").val();
		$.ajax({
				url : '<?php echo site_url('passport/updatepassword')?>',
				type : "post",
				data : {
					'phone' : phone,
					'password' : password,
					'code' : code
				},
				dataType : "json",
				success : function(data) {
					if(data.code)
					{
						leeui.dialog('', data.msg).ok(function(){
							window.location.href="<?php echo site_url('passport/index')?>";
						});
						
					}else{
						leeui.dialog('', data.msg).ok(function(e){
							e.close();
						});;
					}
				}
		});
		return false;
	});
});	 
</script>
 