<!DOCTYPE html>
<head>
  <meta charset="utf-8">
  <!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><![endif]-->
  <title><?php echo $title;?></title>
  <meta name="keywords" content="" />
  <meta name="description" content="" />
  <meta name="viewport" content="width=device-width">        
  <link rel="stylesheet" href="<?=base_url()?>css/bone-x.css">
  <link rel="stylesheet" href="<?=base_url()?>fonts/iconfont.css">
  <script>
        var SITE_URL = '<?php echo site_url(); ?>';
  </script>
</head>
<body class="passport-body">
  <h1 class="passport-logo">
    
  </h1>
  <div class="bo-title-midline">
          <h2 class="title" style="background:#f2f2f2;">注册</h2>
        </div>
<div class="bo-login-box">
    <div class="bo-form bo-m1111">   
      <div  id="form">     
      <div class="bo-form-item">                     
        <div class="bo-input-icon ">
          <input name="email" data-required=true data-format="" class="bo-input bo-form-control" type="text" placeholder="请输入账号">
          <i class="iconfont icon icon-people"></i>
        </div>
      </div>  
        
      <div class="bo-form-item">                     
        <div class="bo-input-icon">
          <input name="password" data-required=true class="bo-input bo-form-control" type="password" placeholder="请输入密码">
           <i class="icon iconfont">&#xe622;</i>
        </div>
        
      </div>    
      
      <div class="bo-form-item">
        <input type="submit" id="submit" class="bo-btn-primary-underline bo-btn-block" value="登 陆">
      </div>
      </div>   
      <div class="bo-form-item bo-tl">
      <a style="float:right;" href="<?php echo site_url('user/login');?>">直接登录</a>
      </div>  
    </div>
  </div>
  <div class="bo-clear"></div>
  <div class="bo-login-footer">
    杭州科益侬网络科技有限公司版权所有
  </div>
  <script src="<?=base_url()?>js/jquery.min.js"></script>
  <script src="<?=base_url()?>js/bone-global.js"></script>
  <script>
      $('#submit').click(function(){
          if(bone.verify.check('#form')){
              var data = bone.verify.getData();
              console.log(data)
              data.role = 0;
              data.name = '';
              $.ajax({
                url: SITE_URL+'/user/registing',
                type: 'post',
                data: data,
                success: function(d){
                  if(d.state){
                    window.location.href = SITE_URL;
                  }
                  else {
                    bone.dialog.init('', data.param);
                  }
                },
                error: function(){
                  bone.dialog.init('', '发生错误请重试');
                },
                dataType: 'json'
              })
          }
      })


  </script>
</body>
